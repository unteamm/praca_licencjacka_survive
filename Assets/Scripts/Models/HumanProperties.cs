﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HumanProperties : CharacterProperties
{
    public string FirstName;
    public string LastName;
    public bool IsHungry;
    public float morale;
    public int ExplorationSkill;
    private float exhaustion;

    public float Morale
    {
        get
        {
            return morale;
        }
        set
        {
            if (value < 0)
            {
                morale = 0;
            }else
            {
                morale = value;
            }
        }
    }

    public float Exhaustion
    {
        set
        {
            exhaustion = value;
            if(exhaustion > 100f)
            {
                exhaustion = 100f;
            }else if(exhaustion < 0f)
            {
                exhaustion = 0f;
            }
            if (exhaustion > 80)
            {
                IsExhausted = true;
            }
            else
            {
                IsExhausted = false;
            }
        }
        get
        {
            return exhaustion;
        }
    }
    public bool IsExhausted;
    public int Age;
    public int HungerDay;

    //Nie musi być serializowane
    public Activity ActualActivity { get; set; }


    public string Avatar;

    public HumanProperties(string _firstName, string _lastName, int _age) : base(Random.Range(10,20), Random.Range(1, 6))
    {
        FirstName = _firstName;
        LastName = _lastName;
        Age = _age;
        IsHungry = false;
        Morale = 3;
        ExplorationSkill = Random.Range(1, 6);
        Exhaustion = 0f;
        HungerDay = 0;
    }

    public HumanProperties(string _firstName, string _lastName, int _age, float _health) : base(_health, Random.Range(1, 6))
    {
        FirstName = _firstName;
        LastName = _lastName;
        Age = _age;
        IsHungry = false;
        Morale = 3;
        ExplorationSkill = Random.Range(1, 6);
        Exhaustion = 0f;
        HungerDay = 0;
    }
     public HumanProperties(string _firstName, string _lastName, int _age, float _health, bool _isHungry, float _morale, int _explorationSkill, float _exhaustion, int _hungerDay, int _fightingSkill, string _avatar, string _id) : base(_health, _fightingSkill)
    {
        FirstName = _firstName;
        LastName = _lastName;
        Age = _age;
        IsHungry = _isHungry;
        Morale = _morale;
        ExplorationSkill = _explorationSkill;
        Exhaustion = _exhaustion;
        HungerDay = _hungerDay;
        Id = _id;
        Avatar = _avatar;

    }

}


public enum Activity { idle, baking, sleeping, scavenging, building, igniting, fighting }
