﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotProperties : CharacterProperties
{
    public string Name;

    public RobotProperties(string _name) : base(100f, Random.Range(1, 6))
    {
        Name = _name;

    }
}
