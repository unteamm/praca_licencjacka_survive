﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharacterProperties  {
    public string Id;
    public float Health;
    public float MaxHealth;
    public int FightingSkill;

    public CharacterProperties(float _health, int _fightingSkill)
    {
        Health = _health;
        MaxHealth = _health;
        FightingSkill = _fightingSkill;
    }
}
