﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[Serializable()]
public class Hint {
    [XmlElement("Id")]
    public string Id { get; set; }
    [XmlElement("Header")]
    public string Header { get; set; }
    [XmlElement("Content")]
    public string Content { get; set; }
    [XmlIgnore]
    public bool Shown = false;
}

[XmlRoot("HintsCollection")]
[XmlInclude(typeof(Hint))]
public class HintsCollection
{
    [XmlArray("Hints")]
    [XmlArrayItem("Hint")]
    public List<Hint> Hints;

    [XmlIgnore]
    public Dictionary<string, Hint> HintsDictionary;

    public void PopulateHintDictionary()
    {
        HintsDictionary = new Dictionary<string, Hint>();
        foreach(Hint h in Hints)
        {
            HintsDictionary.Add(h.Id, h);
        }
    }

    public HintsCollection()
    {
        Hints = new List<Hint>();
    }
}