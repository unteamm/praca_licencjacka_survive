﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[XmlType("Crisis")]
public class Crisis {
    [XmlElement("Id")]
    public int Id { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("Description")]
    public string Description { get; set; }
    [XmlElement("OnBeginCrisisFunction")]
    public string OnBeginCrisisFunction { get; set; }
    [XmlElement("OnEndCrisisFunction")]
    public string OnEndCrisisFunction { get; set; }
    [XmlElement("RewardCrisisFunction")]
    public string RewardCrisisFunction { get; set; }
    [XmlElement("PenaltyCrisisFunction")]
    public string PenaltyCrisisFunction { get; set; }
}

[XmlRoot("CrisesCollection")]
[XmlInclude(typeof(Crisis))]
public class CrisesCollection
{
    [XmlArray("Crises")]
    [XmlArrayItem("Crisis")]
    public List<Crisis> Crises;

    public CrisesCollection()
    {
        Crises = new List<Crisis>();
    }
}