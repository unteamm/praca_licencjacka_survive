﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Interfaces
{
    interface IAnimation
    {
        void PlayIdleAnim();
        void PlayAttackAnim();
        void PlayTakeDamageAnim();
        void PlayDieAnim();
        void PlayTurnToEnemyAnim();
        void PlayTurnToAlliesAnim();
        void PlayWalkToAlliesAnim();
        void PlayWalkToEnemiesAnim();
    }
}
