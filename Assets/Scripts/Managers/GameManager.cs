﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;


    public bool Testing = false;

    bool playerInputEnabled;
    CharacterController selectedCharacter;
    private List<GameObject> equipment;
    public event Action OnSelectedCharacterChange;

    #region Scavengers
    public List<HumanController> Scavengers;

    private float ChanceCheckFrequence = 1f;

    public int GetCountOfScavengers()
    {
        return Scavengers.Count;
    }

    public List<HumanController> GetScavengersList()
    {
        return Scavengers;
    }

    public void AddScavenger(HumanController ch)
    {
        Scavengers.Add(ch);
        ch.CurrentCoroutine = BeginScavenging(ch);
        AvatarPanelController.Instance.DisableAvatar(ch);
        ch.StartCurrentCoroutine();
        //ch.gameObject.SetActive(false);
        ch.HideCharacterGraphics();
    }

    public void RemoveScavenger(HumanController ch)
    {
        Scavengers.Remove(ch);
        AvatarPanelController.Instance.EnableAvatar(ch);
        ch.gameObject.SetActive(true);
        ch.SetActivity(Activity.idle);
        StartCoroutine(ch.StopAndGoBackToCamp());
    }

    IEnumerator BeginScavenging(HumanController scavenger)
    {
        var itemList = ItemDatabase.GetItemList;
        HumanProperties ch = scavenger.GetCharacterProperties();
        while (true)
        {
            int rnd = UnityEngine.Random.Range(0, 100);
            if (rnd <= scavenger.characterProperties.ExplorationSkill)
            {
                GUIManager.instance.AppendLogLine(ch.FirstName + " " + ch.LastName + " found something!");
                int id = itemList[UnityEngine.Random.Range(0, itemList.Count)].Id;
                scavenger.AddItemIdToCollected(id);
            }
            yield return new WaitForSeconds(ChanceCheckFrequence);
        }
    }

    #endregion


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TurnManager.instance.GetStateMachine().ChangeState(TurnManager.instance.GetStateMachine().states["BattleState"]);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            PartyController.instance.CharactersInstances[1].GetComponent<CharacterController>().ReceiveDamage(213);
        }
    }

    public void EnablePlayerInput()
    {
        playerInputEnabled = true;
    }

    public void DisablePlayerInput()
    {
        playerInputEnabled = false;
    }

    public void SetSelectedCharacter(CharacterController _selected)
    {
        if (selectedCharacter != null)
        {
            ((HumanController)(selectedCharacter)).outlineScript.DisableOutline();
        }
        if (_selected == null || _selected.characterProperties.ActualActivity == Activity.idle)
            selectedCharacter = _selected;
        if (selectedCharacter != null)
        {
            ((HumanController)(selectedCharacter)).outlineScript.EnableOutline();
        }
        if (OnSelectedCharacterChange != null)
        {
            OnSelectedCharacterChange.Invoke();
        }
    }

    public CharacterController GetSelectedCharacter()
    {
        return selectedCharacter;
    }

    #region Equipment Functions
    public void AddItemToEquipment(GameObject item)
    {
        equipment.Add(item);
    }

    public List<GameObject> GetItems()
    {
        return equipment;
    }

    public void RemoveAllItems()
    {
        equipment.ForEach(x => DestroyImmediate(x));
        equipment = new List<GameObject>();
    }

    public void RemoveItemFromEquipment(GameObject item)
    {
        equipment.Remove(item);
    }

    public List<GameObject> GetRawFood()
    {
        List<GameObject> foodList = new List<GameObject>();

        foreach (var item in equipment)
        {
            ItemProperties itemProp = item.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
            if (itemProp.Type == ItemProperties.ItemType.RawFood)
            {
                foodList.Add(item);
            }
        }
        return foodList;
    }
    #endregion
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(this);
    }

    public bool InputPossibleCheck()
    {
        if (selectedCharacter != null && playerInputEnabled != false && !EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    void Start()
    {
        equipment = new List<GameObject>();
        Scavengers = new List<HumanController>();
        if (Testing)
        {
            PartyController.instance.GenerateRandomCharacters(4);
        }
    }
}
