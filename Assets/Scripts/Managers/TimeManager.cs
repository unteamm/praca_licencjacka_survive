﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{

    public static TimeManager instance;
    public GameObject TimePanel;
    public Text timeTxt;

    bool BattleMode = false;

    public void UpdateTimeTxt(DateTime _time)
    {
        timeTxt.text = "Time: " + _time.ToString("t");
    }

    DateTime time;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        TimePanel = GameObject.Find("TimePanel");
    }

    public void InitializeNewDay()
    {
        StopAllCoroutines();
        time = new DateTime(1995, 11, 22, 10, 10, 00);
        StartCoroutine(UpdateTime());
    }
    public DateTime GetTime()
    {
        return time;
    }

    public bool TimeEquals(DateTime _time)
    {
        if (time.Hour == _time.Hour && time.Minute == _time.Minute)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator UpdateTime()
    {
        while (true)
        {
            if (BattleMode)
            {
                time = time.AddMinutes(0f);
            }
            else
            {
                time = time.AddMinutes(.1f* Time.timeScale);
                if (time.Hour == 18 && time.Minute == 0)
                {
                    time = time.AddMinutes(1f);
                    DangerLevelEnum dangerLevel = CampManager.instance.CampProperties.DangerLevel;
                    int percentageChanceToAttack = 0;
                    switch (dangerLevel)
                    {
                        case DangerLevelEnum.Low:
                            percentageChanceToAttack = 30;
                            break;
                        case DangerLevelEnum.Moderate:
                            percentageChanceToAttack = 40;
                            break;
                        case DangerLevelEnum.High:
                            percentageChanceToAttack = 60;
                            break;
                        case DangerLevelEnum.VeryHigh:
                            percentageChanceToAttack = 80;
                            break;
                        case DangerLevelEnum.Extreme:
                            percentageChanceToAttack = 100;
                            break;
                        default:
                            break;
                    }
                    if (UnityEngine.Random.Range(0, 100) <= percentageChanceToAttack)
                        TurnManager.instance.GetStateMachine().ChangeState(TurnManager.instance.GetStateMachine().states["BattleState"]);
                }
            }
            UpdateTimeTxt(time);
            yield return new WaitForSeconds(.1f);
        }
    }

    public void ShowTimePanel()
    {
        TimePanel.SetActive(true);
    }

    public void HideTimePanel()
    {
        TimePanel.SetActive(false);
    }

    public void SetNormalSpeed()
    {
        Time.timeScale = 1f;
    }

    public void SpeedUpTwice()
    {
        Time.timeScale = 2f;
    }

    public void SpeedUp()
    {
        Time.timeScale = 5f;
    }

    public void StopTime()
    {
        Time.timeScale = 0f;
    }

    public void BeginBattleMode()
    {
        BattleMode = true;
    }

    public void EndBattleMode()
    {
        BattleMode = false;
    }
}
