﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CampManager : MonoBehaviour
{
    public static CampManager instance;

    public Dictionary<Transform, bool> SpawnPoints;
    public Dictionary<Transform, bool> EnemySpawnPoints;
    public CampNodeProperties CampProperties;
    public GameObject CampGameObject;

    public int Traps = 0;
    
    void Awake()
    {
        instance = this;
        LoadSpawnPoints();
    }

    public void LoadSpawnPoints()
    {
        SpawnPoints = new Dictionary<Transform, bool>();
        foreach (GameObject spawnPoint in GameObject.FindGameObjectsWithTag("SpawnPoint"))
        {
            SpawnPoints.Add(spawnPoint.transform, false);
        }

        EnemySpawnPoints = new Dictionary<Transform, bool>();
        foreach (GameObject spawnPoint in GameObject.FindGameObjectsWithTag("EnemySpawnPoint"))
        {
            EnemySpawnPoints.Add(spawnPoint.transform, false);
        }
    }

    public Transform GetFreeSpawnPoint()
    {

        foreach (KeyValuePair<Transform, bool> spawnPoint in SpawnPoints.OrderBy(elem => Guid.NewGuid()))
        {
            if (!spawnPoint.Value)
            {
                SpawnPoints[spawnPoint.Key] = true;
                return spawnPoint.Key;
            }
        }
        return null;
    }

    public Transform GetFreeEnemySpawnPoint()
    {
        foreach (KeyValuePair<Transform, bool> spawnPoint in EnemySpawnPoints)
        {
            if (!spawnPoint.Value)
            {
                EnemySpawnPoints[spawnPoint.Key] = true;
                return spawnPoint.Key;
            }
        }
        return null;
    }

    public void ReloadCamp(bool areChangingCamp = true)
    {
        var newCampGameObject = Instantiate(Resources.Load(getPrefabPath(), typeof(GameObject)),Vector3.zero,Quaternion.identity) as GameObject;
        newCampGameObject.transform.position = CampGameObject.transform.position;

        DestroyImmediate(CampGameObject);

        newCampGameObject.name = "Camp";
        CampGameObject = newCampGameObject;
        LoadSpawnPoints();

        PartyController.instance.DespawnAllCharacters();
        PartyController.instance.SpawnAllCharacters(areChangingCamp);
        Pathfinding.instance.RegenerateGrid();
    }

    private string getPrefabPath()
    {
        switch (CampProperties.TerrainType)
        {
            case TerrainTypeEnum.City:
                {
                    return "Prefabs/Camps/City";
                }
            case TerrainTypeEnum.Forest:
                {
                    return "Prefabs/Camps/Forest";
                }
            case TerrainTypeEnum.Lake:
                {
                    return "Prefabs/Camps/Lake";
                }
            case TerrainTypeEnum.Village:
                {
                    return "Prefabs/Camps/Village";
                }
            case TerrainTypeEnum.Barn:
                {
                    return "Prefabs/Camps/Barn";
                }
            case TerrainTypeEnum.Scrapyard:
                {
                    return "Prefabs/Camps/Scrapyard";
                }
            default:
                return "";
        }
    }
}
