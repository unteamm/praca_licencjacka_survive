﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
    public static MenuManager instance;

    private void Awake()
    {
        instance = this;
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene("_HERO_CHOICE_");
    }

    public void StartGame()
    {
        SceneManager.LoadScene("_CAMP_MAIN_");
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("_MAIN_MENU");
    }

    public void LoadGame()
    {
        if (File.Exists("save.dat"))
        {
            PlayerPrefs.SetInt("Load", 1);
            StartGame();
        }
        else if (PlayerPrefs.HasKey("Load"))
            PlayerPrefs.DeleteKey("Load");

    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
