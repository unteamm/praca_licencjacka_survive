﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {
    public AudioClip MenuClip;
    public AudioClip CampClip;
    public AudioClip VictoryClip;
    public AudioClip FailureClip;

    AudioSource audioSource;

    AudioClip actualClip;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        audioSource = GetComponent<AudioSource>();
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        audioSource.loop = true;
        if (scene.name.Contains("_CAMP_MAIN"))
        {
            if(actualClip!=CampClip)
                actualClip = CampClip;
        }
        else if (scene.name.Contains("THE_END"))
        {
            if (actualClip != VictoryClip)
                actualClip = VictoryClip;

        }else if (scene.name.Contains("GAME_OVER"))
        {
            if (actualClip != FailureClip)
                actualClip = FailureClip;
        }
        else
        {
            if (actualClip != MenuClip)
                actualClip = MenuClip;
        }
        PlayMusic();
    }

    void PlayMusic()
    {
        if (audioSource.clip != actualClip)
        {
            audioSource.clip = actualClip;
            audioSource.Play();
        }
    }
}
