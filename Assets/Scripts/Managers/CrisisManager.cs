﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class CrisisManager : MonoBehaviour {
    public static CrisisManager instance;

    public CrisesCollection CrisesCollection;
    public Crisis ActiveCrisis;

    public Dictionary<string, Func<bool>> CrisesLogicFunctions;
    public Dictionary<string, Action> CrisesPenaltyRewardFunctions;

    [Header("GUI")]
    public GameObject CrisisPanel;
    public GameObject CrisisSummaryPanel;
    public Text SummaryText;
    public Text NameText;
    public Text DescriptionText;
    public Text SuccessRewardText;
    public Text FailedRewardText;

    void Awake()
    {
        instance = this;
        HideCrisisPanel();
        CrisesLogicFunctions = new Dictionary<string, Func<bool>>();
        CrisesPenaltyRewardFunctions = new Dictionary<string, Action>();
        CrisesLogicFunctions.Add("CheckAmountOfFood", CheckAmountOfFood);
        CrisesLogicFunctions.Add("CheckNumberOfWeapon", CheckNumberOfWeapon);
        CrisesLogicFunctions.Add("CheckHealthOfCharacters", CheckHealthOfCharacters);
        CrisesLogicFunctions.Add("CheckHungerOfCharacters", CheckHungerOfCharacters);
        CrisesLogicFunctions.Add("CheckNumberOfMedicalKits", CheckNumberOfMedicalKits);
        CrisesLogicFunctions.Add("CheckNumberOfClothes", CheckNumberOfClothes);
        CrisesLogicFunctions.Add("CheckNumberOfScavengers", CheckNumberOfScavengers);
        CrisesLogicFunctions.Add("CheckCharactersExhaustion", CheckCharactersExhaustion);

        CrisesPenaltyRewardFunctions.Add("IncreasePartyMorale", IncreasePartyMorale);
        CrisesPenaltyRewardFunctions.Add("DecreasePartyMorale", DecreasePartyMorale);

    }

    void Start()
    {
        CrisesCollection = new CrisesCollection();
        CrisesCollection = LoadCrisesList();
    }

    public string CheckCrisis()
    {
        if (CrisesLogicFunctions[ActiveCrisis.OnEndCrisisFunction].Invoke())
        {
            CrisesPenaltyRewardFunctions[ActiveCrisis.RewardCrisisFunction].Invoke();
            return "Previous mission succesfull - " + Addons.ToLowercaseNamingConvention(ActiveCrisis.RewardCrisisFunction, true);
        }else
        {
            CrisesPenaltyRewardFunctions[ActiveCrisis.PenaltyCrisisFunction].Invoke();
            return "Previous mission failed - " + Addons.ToLowercaseNamingConvention(ActiveCrisis.PenaltyCrisisFunction, true);
        }
    }

    public Crisis GetRandomCrisis()
    {
        int random = UnityEngine.Random.Range(0, CrisesCollection.Crises.Count);
        return CrisesCollection.Crises[random];
    }

    public CrisesCollection LoadCrisesList()
    {
        TextAsset xmlData = (TextAsset)Resources.Load("Crises", typeof(TextAsset));
        
        var serializer = new XmlSerializer(typeof(CrisesCollection));
        // var stream = new FileStream(Path.Combine(Application.dataPath, "Crises.xml"), FileMode.Open);
        var stream = new StringReader(xmlData.text);
        var container = serializer.Deserialize(stream) as CrisesCollection;
        stream.Close();
        return container;
    }

    void UpdateCrisisPanel()
    {
        NameText.text = ActiveCrisis.Name;
        DescriptionText.text = ActiveCrisis.Description;
        SuccessRewardText.text = "Reward: " + Addons.ToLowercaseNamingConvention(ActiveCrisis.RewardCrisisFunction, true);
        if (ActiveCrisis.PenaltyCrisisFunction != "")
        {
            FailedRewardText.text = "Penalty: " + Addons.ToLowercaseNamingConvention(ActiveCrisis.PenaltyCrisisFunction, true);
        }else
        {
            FailedRewardText.text = "Penalty: -";
        }
    }

    internal void SetNewActiveCrisis(string previousResult)
    {
        ActiveCrisis = GetRandomCrisis();
        if (CrisesLogicFunctions.ContainsKey(ActiveCrisis.OnBeginCrisisFunction))
        {
            CrisesLogicFunctions[ActiveCrisis.OnBeginCrisisFunction].Invoke();
        }
        ShowCrisisPanel(previousResult);
    }

    public void ShowCrisisPanel(string previousResult)
    {
        CrisisPanel.SetActive(true);
        if(previousResult != "")
        {
            CrisisSummaryPanel.SetActive(true);
            SummaryText.text = previousResult;
        }else
        {
            CrisisSummaryPanel.SetActive(false);
        }
        UpdateCrisisPanel();
    }

    public void HideCrisisPanel()
    {
        CrisisPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            if(CrisisPanel.active == true)
            {
                HideCrisisPanel();
            }
            else
            {
                ShowCrisisPanel("");
            }
        }
    }

    public void IncreasePartyMorale()
    {
        foreach(HumanProperties cp in PartyController.instance.characters)
        {
            cp.Morale++;
        }
    }

    public void DecreasePartyMorale()
    {
        foreach (HumanProperties cp in PartyController.instance.characters)
        {
            cp.Morale--;
        }
    }

    #region Crisis conditions

    public bool CheckHealthOfCharacters()
    {
        foreach (HumanController character in PartyController.instance.CharactersInCamp)
        {
            if (character.characterProperties.Health < 10)
            {
                return false;
            }
        }
        return true;
    }

    public bool CheckHungerOfCharacters()
    {
        foreach (HumanController character in PartyController.instance.CharactersInCamp)
        {
            if (character.characterProperties.IsHungry)
            {
                return false;
            }
        }
        return true;
    }

    public bool CheckNumberOfMedicalKits()
    {
        int counter = 0;
        foreach (GameObject item in GameManager.instance.GetItems())
        {
            ItemProperties itemProp = item.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
            if (itemProp.Type == ItemProperties.ItemType.Health)
            {
                counter++;
            }
        }

        if (counter > 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckNumberOfClothes()
    {
        int counter = 0;
        foreach (GameObject item in GameManager.instance.GetItems())
        {
            ItemProperties itemProp = item.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
            if (itemProp.Type == ItemProperties.ItemType.Armor || itemProp.Type == ItemProperties.ItemType.Boots || itemProp.Type == ItemProperties.ItemType.Helmet)
            {
                counter++;
            }
        }

        if (counter > 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckNumberOfScavengers()
    {
       if (GameManager.instance.GetScavengersList().Count == 0)
       {
          return false;
       }
        return true;
    }

    public bool CheckCharactersExhaustion()
    {
        foreach (HumanController character in PartyController.instance.CharactersInCamp)
        {
            if (character.characterProperties.IsExhausted)
            {
                return false;
            }
        }
        return true;
    }

    public bool CheckNumberOfWeapon()
    {
        int counter = 0;
        foreach (GameObject item in GameManager.instance.GetItems())
        {
            ItemProperties itemProp = item.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
            if (itemProp.Type == ItemProperties.ItemType.Weapon)
            {
                counter++;
            }
        }

        if (counter > 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    public bool CheckAmountOfFood()
    {
        int counter = 0;
        foreach(GameObject item in GameManager.instance.GetItems())
        {
            ItemProperties itemProp = item.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
            if (itemProp.Type== ItemProperties.ItemType.Food)
            {
                counter++;
            }
        }

        if(counter > 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}


public static class Addons
{
    public static string ToLowercaseNamingConvention(this string s, bool toLowercase)
    {
        if (toLowercase)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(s, " ").ToLower();
        }
        else
            return s;
    }
}