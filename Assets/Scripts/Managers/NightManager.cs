﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NightManager : MonoBehaviour
{
    public static NightManager instance;

    public CharacterController CurrentDefender;
    public CharacterController CurrentAttacker;

    public GameObject robotPrefab;
    public List<RobotController> Robots;
    public List<HumanController> ScavengersInCamp
    {
        get
        {
            return PartyController.instance.CharactersInCamp;
        }
    }

    //public event Action OnRobotDeath;
    //wartownik pilnujacy obozu
    public bool isFightOver = false;

    void Awake()
    {
        instance = this;
    }

    void ClearLists()
    {
        Robots = new List<RobotController>();
        //ScavengersInCamp = new List<HumanController>();
    }

    void GetScavengers()
    {
        foreach (var character in PartyController.instance.CharactersInCamp)
        {
            ScavengersInCamp.Add(character);
        }
    }

    public IEnumerator StartFightEnumerator()
    {
        foreach (var character in PartyController.instance.CharactersInstances)
        {
            character.SetActive(true);
            character.GetComponent<HumanController>().ShowHealthBar();
            if (character.GetComponent<HumanController>().characterProperties.ActualActivity == Activity.scavenging)
                character.GetComponent<HumanController>().WasScavengingInterrupted = true;
            yield return StartCoroutine(character.GetComponent<HumanController>().StopAndGoBackToCamp());
            character.GetComponent<HumanController>().SetActivity(Activity.fighting);
        }
        Debug.Log("Start Nocy");
        GUIManager.instance.AppendLogLine("Enemies are incoming!");
        ClearLists();
        SpawnEnemies();
        GetScavengers();
        Debug.Log("Player count: " + ScavengersInCamp.Count);
        yield return StartCoroutine(Fight());
    }

    private int generateEnemiesCount()
    {
        switch (CampManager.instance.CampProperties.DangerLevel)
        {
            case DangerLevelEnum.Low:
                return UnityEngine.Random.Range(1, 2);
            case DangerLevelEnum.Moderate:
                return UnityEngine.Random.Range(1, 2);
            case DangerLevelEnum.High:
                return UnityEngine.Random.Range(2, 3);
            case DangerLevelEnum.VeryHigh:
                return UnityEngine.Random.Range(3, 4);
            case DangerLevelEnum.Extreme:
                return UnityEngine.Random.Range(4, 5);
        }
        return 0;
    }
    public void SpawnEnemies()
    {
        CampManager.instance.LoadSpawnPoints();
        int enemiesCount = generateEnemiesCount();
        //Traps
        int trapCount = CampManager.instance.Traps;
        while (enemiesCount > 0 && trapCount > 0)
        {
            enemiesCount--;
            trapCount--;
            CampManager.instance.Traps--;
            Debug.Log("Robot wpadl w pulapke");
            GUIManager.instance.AppendLogLine("Robot has get into trap!");
        }
        for (int i = 0; i < enemiesCount; i++)
        {
            HumanProperties properties = new HumanProperties("Robot #" + UnityEngine.Random.Range(0, 999), "test", i);
            properties.ActualActivity = Activity.fighting;

            GameObject characterGameObject = Instantiate(robotPrefab, CampManager.instance.GetFreeEnemySpawnPoint().position, Quaternion.identity) as GameObject;

            characterGameObject.GetComponent<RobotController>().SetCharacterProperties(properties);
            Robots.Add(characterGameObject.GetComponent<RobotController>());
        }
        Debug.Log("Spawned " + enemiesCount + " enemies");
    }

    public void DespawnEnemies()
    {
        for (int i = Robots.Count - 1; i >= 0; i--)
        {
            Destroy(Robots[i].gameObject);
            Robots.RemoveAt(i);
        }
    }

    IEnumerator AttackGroup(List<CharacterController> attackers, List<CharacterController> defenders)
    {
        foreach (var attacker in attackers)
        {
            CurrentAttacker = attacker;
            var aliveDefenders = defenders.Where(x => x.GetCharacterProperties().Health > 0).ToList();
            if (aliveDefenders.Count == 0)
                continue;

            var defender = aliveDefenders[UnityEngine.Random.Range(0, aliveDefenders.Count)];
            CurrentDefender = defender;
            //yield return new WaitForSeconds(1f);

            if(attacker is HumanController)
                attacker.GoTo(defender.transform.position + new Vector3(2f,0));
            else
                attacker.GoTo(defender.transform.position - new Vector3(2f, 0));

            while (!attacker.CheckIfTargetArrived())
            {
                yield return new WaitForSeconds(0.2f);
            }

            attacker.AttackCharacter(defender);
            yield return new WaitForSeconds(1.5f);
            if (defender.GetCharacterProperties().Health > 0)
                defender.SetIdleAnim();
            attacker.SetIdleAnim();

            yield return new WaitForSeconds(0.75f);

            attacker.ReturnToDefaultPosition();

            while (!attacker.CheckIfTargetArrived())
            {
                yield return new WaitForSeconds(0.2f);
            }
            attacker.SetIdleAnim();
        }
        RemoveDeadFromList();
    }

    public void RemoveDeadFromList()
    {
        for (int i = Robots.Count - 1; i >= 0; i--)
        {
            if (Robots[i].GetCharacterProperties().Health <= 0)
            {
                Robots.Remove(Robots[i]);
            }
        }

        for (int i = ScavengersInCamp.Count - 1; i >= 0; i--)
        {
            if (ScavengersInCamp[i].GetCharacterProperties().Health <= 0)
            {
                PartyController.instance.RemoveHumanControllerFromAllLists(ScavengersInCamp[i]);
                ScavengersInCamp.Remove(ScavengersInCamp[i]);
            }

        }
    }


    IEnumerator Fight()
    {
        while (true)
        {
            RemoveDeadFromList();
            yield return new WaitForSeconds(1);
            if (ScavengersInCamp.Count <= 0)
            {
                yield return new WaitForSeconds(3f);
                SceneManager.LoadScene("GAME_OVER_SCENE");
            }
            if (Robots.Count <= 0)
            {
                GUIManager.instance.AppendLogLine("End of the fight");
                ScavengersInCamp.ForEach(
                    x =>
                    {
                        HumanController hC = x.GetComponent<HumanController>();
                        hC.SetActivity(Activity.idle);
                        hC.HideHealthBar();
                        hC.SetIdleAnim();
                        if(hC.WasScavengingInterrupted)
                        {
                            Debug.Log("Scavenging was interrupted, trying to go back to scavenging");
                            hC.WasScavengingInterrupted = false;
                            GameObject.Find("ExitCampPrefab").GetComponent<ExitController>().StartContinueScavenging(hC);
                        }

                    });
                GameManager.instance.Scavengers = new List<HumanController>();
                Debug.Log("Koniec walki");
                isFightOver = true;
                break;
            }
                yield return StartCoroutine(AttackGroup(ScavengersInCamp.Cast<CharacterController>().ToList(), Robots.Cast<CharacterController>().ToList()));
                yield return StartCoroutine(AttackGroup(Robots.Cast<CharacterController>().ToList(), ScavengersInCamp.Cast<CharacterController>().ToList()));
        }
    }
}
