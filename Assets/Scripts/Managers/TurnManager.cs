﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour {
    public static TurnManager instance;
    int numberOfTurn;

    public event Action OnTurnStateChange;

    FSM stateMachine;

    void Awake()
    {
        instance = this;
        stateMachine = new FSM(GetComponents<FSMState>());
        stateMachine.AddTransition(stateMachine.states["NewCampState"], stateMachine.states["PlayerTurnState"]);
        stateMachine.AddTransition(stateMachine.states["PlayerTurnState"], stateMachine.states["NightState"]);
        stateMachine.AddTransition(stateMachine.states["NightState"], stateMachine.states["PlayerTurnState"]);
        stateMachine.AddTransition(stateMachine.states["BattleState"], stateMachine.states["PlayerTurnState"]);
    }

    void Start()
    {
        NumberOfTurn = 0;
    }

    void Update()
    {
        stateMachine.Run();

    }

    #region Setters and getters

    public int NumberOfTurn
    {
        get
        {
            return numberOfTurn;
        }

        set
        {
            numberOfTurn = value;
        }
    }

    public FSM GetStateMachine()
    {
        return stateMachine;
    }
    #endregion 
}
