﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUICharacterSelectManager : MonoBehaviour {

    public GameObject ChosenCharactersPanel;

    public GameObject CharacterPresentationPrefab;

    private void Awake()
    {
        PartyController.instance.GenerateRandomCharacters(4);
        ShowGeneratedCharacters();
    }
    private void Start()
    {
        TutorialManager.instance.ShowHintById("ChSel_1");
    }

    public void ShowGeneratedCharacters()
    {
        for (int i = 0; i < PartyController.instance.characters.Count; i++)
        {
            GameObject characterPresentation = (GameObject)Instantiate(CharacterPresentationPrefab, ChosenCharactersPanel.transform,false);
            GameObject character = PartyController.instance.charactersPrefabs[PartyController.instance.characters[i].Id];
            characterPresentation.transform.FindChild("Image").GetComponent<Image>().sprite = character.transform.FindChild("Graphics").GetChild(0).GetComponent<SpriteRenderer>().sprite;
            Transform statsPresentation = characterPresentation.transform.FindChild("Stats");
            statsPresentation.GetChild(0).GetComponent<Text>().text = PartyController.instance.characters[i].FirstName + " " + PartyController.instance.characters[i].LastName;
            statsPresentation.GetChild(1).GetComponent<Text>().text = "Health: " + PartyController.instance.characters[i].Health;
            statsPresentation.GetChild(2).GetComponent<Text>().text = "Fighting skill: " + PartyController.instance.characters[i].FightingSkill;
            statsPresentation.GetChild(3).GetComponent<Text>().text = "Exploration skill: " + PartyController.instance.characters[i].ExplorationSkill;

        }
    }

}
