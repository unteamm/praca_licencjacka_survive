﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    public static TutorialManager instance;

    public GameObject HintPanel;
    Text hintHeader;
    Text hintContent;
    Button closeHintButton;
    Button disableHintsButton;
    bool hintsEnabled = true;

    public HintsCollection HintsCollection;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else Destroy(this);
        HintsCollection = new HintsCollection();
        HintsCollection = LoadHintsList();
        HintsCollection.PopulateHintDictionary();
        OnLevelWasLoaded(0);
    }

    public void OnLevelWasLoaded(int level)
    {
        if (GameObject.FindGameObjectWithTag("HintGUI") != null)
        {
            HintPanel = GameObject.FindGameObjectWithTag("HintGUI");
            foreach (var child in HintPanel.GetComponentsInChildren<Text>())
            {
                if (child.name == "Header")
                {
                    hintHeader = child;
                }
                else if (child.name == "Content")
                {
                    hintContent = child;
                }
            }
            foreach (var child in HintPanel.GetComponentsInChildren<Button>())
            { 
                if (child.name == "DisableHintsBtn")
                {
                    disableHintsButton = child;
                    disableHintsButton.onClick.RemoveAllListeners();
                    disableHintsButton.onClick.AddListener(DisableHints);
                }
                else if (child.name == "CloseBtn")
                {
                    closeHintButton = child;
                    closeHintButton.onClick.RemoveAllListeners();
                    closeHintButton.onClick.AddListener(CloseHint);
                }
            }
            HintPanel.SetActive(false);
        }
    }


    void CloseHint()
    {
        HintPanel.SetActive(false);
    }

    void DisableHints()
    {
        hintsEnabled = false;
        HintPanel.SetActive(false);
    }

    public void ShowHintById(string id)
    {
        if (hintsEnabled)
        {
            Hint hint = HintsCollection.HintsDictionary[id];
            if (hint.Shown == false)
            {
                hintContent.text = hint.Content;
                hintHeader.text = hint.Header;
                HintPanel.SetActive(true);
                hint.Shown = true;
            }
        }
    }

    public HintsCollection LoadHintsList()
    {
        TextAsset xmlData = (TextAsset)Resources.Load("Hints", typeof(TextAsset));

        var serializer = new XmlSerializer(typeof(HintsCollection));
        var stream = new StringReader(xmlData.text);
        var container = serializer.Deserialize(stream) as HintsCollection;
        stream.Close();
        return container;
    }
    
    

}
