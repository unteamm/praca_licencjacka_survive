﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {
    public static GUIManager instance;

    public GameObject DeveloperPanel;
    public Text TurnStateTxt;
    public Text TurnNumberTxt;


    [Header("Interact Panel")]
    public GameObject InteractPanel;
    public Text Header;
    public Button Button1;
    public Button Button2;
    public Button Button3;
    public Button Button4;

    [Header("Character Panel")]
    public GameObject CharacterPanel;
    public Transform CharacterPropertiesPanel;

    [Header("Console")]
    public GameObject Console;
    public Text logTextArea;

    Text fNameTxt;
    Text lNameTxt;
    Text ageTxt;
    Text actionPointsTxt;
    Text isHungryTxt;
    Text moraleTxt;
    Text exhaustionTxt;
    Text healthTxt;
    Text fightingSkillsTxt;
    Text explorationSkillsTxt;

    Text messageTxt;
    Text battleLogTxt;

    [Header("Menu")]
    public GameObject MainMenuPanel;


    void Awake()
    {
        instance = this;

        foreach(var child in CharacterPropertiesPanel.GetComponentsInChildren<Text>())
        {
            if (child.name == "IsHungryTxt")
            {
                isHungryTxt = child;
            }
            else if(child.name == "MoraleTxt")
            {
                moraleTxt = child;
            }
            else if(child.name == "ExhaustionTxt")
            {
                exhaustionTxt = child;
            }
            else if(child.name == "HealthTxt")
            {
                healthTxt = child;
            }
            else if(child.name == "FightingSkillsTxt")
            {
                fightingSkillsTxt = child;
            }
            else if(child.name == "ExplorationSkillsTxt")
            {
                explorationSkillsTxt = child;
            }
        }

  

        CharacterPanel.SetActive(false);
        //TurnManager.instance.OnTurnStateChange += UpdateTurnState;
    }

    private void Start()
    {
        GameManager.instance.OnSelectedCharacterChange += OnCharacterChange;
        TutorialManager.instance.ShowHintById("Camp_Overview_1");
    }

    private void OnDestroy()
    {
        GameManager.instance.OnSelectedCharacterChange -= OnCharacterChange;
    }

    public void OnCharacterChange()
    {
        if (GameManager.instance.GetSelectedCharacter() != null)
        {
            AppendLogLine(GameManager.instance.GetSelectedCharacter().name + " selected");
        }
    }

    #region Console

    const int scrollbackSize = 20;
    Queue<string> scrollback = new Queue<string>(scrollbackSize);
    public string[] log { get; private set; }


    public void AppendLogLine(string line)
    {
        if(scrollback.Count >= scrollbackSize)
        {
            scrollback.Dequeue();
        }
        scrollback.Enqueue(line);

        log = scrollback.ToArray();
        updateLogStr(log);
    }

    void updateLogStr(string[] newLog)
    {
        if(newLog == null)
        {
            logTextArea.text = "";
        }else
        {
            logTextArea.text = string.Join("\n", newLog);
        }
    }

    #endregion


    #region Interact Panel
    public void ShowInteractPanel(InteractPanelDetails _details)
    {
        Button1.gameObject.SetActive(false);
        Button2.gameObject.SetActive(false);
        Button3.gameObject.SetActive(false);
        Button4.gameObject.SetActive(false);

        Button1.onClick.RemoveAllListeners();
        Button1.onClick.AddListener(_details.button1Details.action);
        Button1.onClick.AddListener(HideInteractPanel);
        Button1.transform.GetChild(0).GetComponent<Text>().text = _details.button1Details.buttonTitle;
        Button1.gameObject.SetActive(true);

        Header.text = _details.Header;

        if (_details.button2Details != null)
        {
            Button2.onClick.RemoveAllListeners();
            Button2.onClick.AddListener(_details.button2Details.action);
            Button2.onClick.AddListener(HideInteractPanel);
            Button2.transform.GetChild(0).GetComponent<Text>().text = _details.button2Details.buttonTitle;
            Button2.gameObject.SetActive(true);
        }
        if (_details.button3Details != null)
        {
            Button3.onClick.RemoveAllListeners();
            Button3.onClick.AddListener(_details.button3Details.action);
            Button3.onClick.AddListener(HideInteractPanel);
            Button3.transform.GetChild(0).GetComponent<Text>().text = _details.button3Details.buttonTitle;
            Button3.gameObject.SetActive(true);
        }
        if (_details.button4Details != null)
        {
            Button4.onClick.RemoveAllListeners();
            Button4.onClick.AddListener(_details.button3Details.action);
            Button4.onClick.AddListener(HideInteractPanel);
            Button4.transform.GetChild(0).GetComponent<Text>().text = _details.button4Details.buttonTitle;
            Button4.gameObject.SetActive(true);
        }
        else
        {
            Button4.onClick.AddListener(HideInteractPanel);
            Button4.transform.GetChild(0).GetComponent<Text>().text = "Cancel";
            Button4.gameObject.SetActive(true);
        }
        InteractPanel.SetActive(true);
    }

    public void HideInteractPanel()
    {
        InteractPanel.SetActive(false);
    }

    #endregion

    #region Character Panel
    public void UpdateCharacterPanel(HumanProperties humanProp)
    {
        CharacterPanel.SetActive(true);
        if (humanProp != null)
        {
            if (humanProp.IsHungry) {
                isHungryTxt.text = humanProp.FirstName + " is hungry!";
            }
            else
            {
                isHungryTxt.text = humanProp.FirstName + " is not hungry";
            }
            moraleTxt.text = "Morale: " + humanProp.Morale.ToString();
            if (humanProp.IsExhausted)
            {
                exhaustionTxt.text = humanProp.FirstName + " is exhausted";
            }
            else
            {
                exhaustionTxt.text = humanProp.FirstName + " is rested ";
            }
            healthTxt.text = "Heatlh: " + humanProp.Health.ToString();
            fightingSkillsTxt.text = "Fighting skill: " + humanProp.FightingSkill.ToString();
            explorationSkillsTxt.text = "Exploration skill: " + humanProp.ExplorationSkill.ToString();
        }
        else
        {
            CharacterPanel.SetActive(false);
        }
    }

    public void HideCharacterPanel()
    {
        CharacterPanel.SetActive(false);
    }

    #endregion

    public void ShowHideMainMenu()
    {
        if(MainMenuPanel.active == true)
        {
            MainMenuPanel.SetActive(false);
        }
        else
        {
            MainMenuPanel.SetActive(true);
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("_MAIN_MENU");
    }

    public void LoadGame()
    {
        if (File.Exists("save.dat"))
        {
            PlayerPrefs.SetInt("Load", 1);
            SceneManager.LoadScene("_CAMP_MAIN_");
        }
        else if (PlayerPrefs.HasKey("Load"))
            PlayerPrefs.DeleteKey("Load");
    }
}

public class InteractPanelDetails
{
    public string Header;
    public ButtonDetails button1Details;
    public ButtonDetails button2Details;
    public ButtonDetails button3Details;
    public ButtonDetails button4Details;
}

public class ButtonDetails
{
    public string buttonTitle;
    public UnityAction action;
}
