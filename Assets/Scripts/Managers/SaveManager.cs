﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    private GameObject trash;
    public static SaveManager instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        trash = GameObject.Find("Trash");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveGame();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadGame("save.dat");
        }
    }

    public void SaveGame()
    {
        Save save = new Save();
        PartyController.instance.characters.ForEach(
            x =>
            {
                Vector3 pos = PartyController.instance.CharactersInstances
                    .First(d => d.GetComponent<CharacterController>().characterProperties == x).transform.position;

                save.HumansSaves.Add(new HumanSave() { HumanProperties = x, PositionX = pos.x, PositionY = pos.y });
            });

        save.MapInformation = MapManager.instance.MapInformation;
        save.Traps = CampManager.instance.Traps;
        save.InventoryItems = GameManager.instance.GetItems()
            .Select(x =>
           {
               IntentoryItemSave item = new IntentoryItemSave();
               item.ItemId = (x.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
               if ((x.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).IsStackable)
                   item.Count = int.Parse(x.GetComponentInChildren<Text>().text);
               return item;
           }).ToList();
        save.PlayersEquipments = GetPlayersEquipmentsIds();

        Serialize(save);
        Debug.Log("Game Saved");
    }

    public void LoadGame(string fileName)
    {
        Save save = Deserialize(fileName);
        if (save != null)
            Debug.Log("Game Loaded");
        else
        {
            Debug.Log("Loading Error");
            return;
        }
        //Ustawienie mapy
        MapManager.instance.MapInformation = save.MapInformation;
        CampManager.instance.CampProperties = save.MapInformation.ListaProperties.Where(x => x.Id == save.MapInformation.PlayerCurrentCampId).First();
        MapManager.instance.InstantiateFromMapInformation();

        //Ustawienie postaci i obozu
        PartyController.instance.characters = save.HumansSaves.Select(x => x.HumanProperties).ToList();
        CampManager.instance.Traps = save.Traps;
        CampManager.instance.ReloadCamp(false);
        PartyController.instance.CharactersInstances.ForEach(
            x =>
            {
                CharacterController characterController = x.GetComponent<CharacterController>();
                HumanSave current = save.HumansSaves.Where(y => y.HumanProperties.Id == characterController.characterProperties.Id).First();
                Vector3 pos = new Vector2(current.PositionX, current.PositionY);
                characterController.DefaultPosition = pos;
                x.transform.position = pos;
            });

        //Ustawienie itemow z ekwipunku
        SetPlayersEq(save.PlayersEquipments);

        //Ustawienie itemow z inventory
        GameManager.instance.RemoveAllItems();
        save.InventoryItems.ForEach(x =>
        {
            GameObject go = ItemFactory.CreateItem(x.ItemId);
            if ((go.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).IsStackable)
                go.GetComponentInChildren<Text>().text = x.Count.ToString();
        });

        TimeManager.instance.InitializeNewDay();
    }

    private void Serialize(Save save)
    {
        FileStream fs = new FileStream("save.dat", FileMode.Create);

        BinaryFormatter formatter = new BinaryFormatter();
        try
        {
            formatter.Serialize(fs, save);
        }
        catch (SerializationException e)
        {
            Debug.Log(e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }
    }

    private Save Deserialize(string fileName)
    {
        FileStream fs = new FileStream(fileName, FileMode.Open);
        Save save;
        BinaryFormatter formatter = new BinaryFormatter();
        try
        {
            save = (Save)formatter.Deserialize(fs);
        }
        catch (SerializationException e)
        {
            Debug.Log(e.Message);
            throw;
        }
        finally
        {
            fs.Close();
        }
        return save;
    }

    public List<PlayerEquipment> GetPlayersEquipmentsIds(bool destroyItem = false)
    {
        List<PlayerEquipment> playerEquipment = new List<PlayerEquipment>();
        foreach (var character in PartyController.instance.CharactersInCamp)
        {
            PlayerEquipment playerEq = new PlayerEquipment();
            playerEq.Id = character.GetComponent<HumanController>().characterProperties.Id;

            GameObject HelmetObject = character.GetComponent<HumanController>().Helmet;
            if (HelmetObject != null)
            {
                playerEq.HelmetId = (HelmetObject.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
                if (destroyItem) DestroyImmediate(character.GetComponent<HumanController>().Helmet);
            }

            GameObject ArmorObject = character.GetComponent<HumanController>().Armor;
            if (ArmorObject != null)
            {
                playerEq.ArmorId = (ArmorObject.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
                if (destroyItem) DestroyImmediate(character.GetComponent<HumanController>().Armor);
            }

            GameObject LeftHandWeaponObject = character.GetComponent<HumanController>().LeftHandWeapon;
            if (LeftHandWeaponObject != null)
            {
                playerEq.LeftHandWeaponId = (LeftHandWeaponObject.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
                if (destroyItem) DestroyImmediate(character.GetComponent<HumanController>().LeftHandWeapon);
            }

            GameObject RightHandWeaponObject = character.GetComponent<HumanController>().RightHandWeapon;
            if (RightHandWeaponObject != null)
            {
                playerEq.RightHandWeaponId = (RightHandWeaponObject.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
                if (destroyItem) DestroyImmediate(character.GetComponent<HumanController>().RightHandWeapon);
            }

            GameObject BootsObject = character.GetComponent<HumanController>().Boots;
            if (BootsObject != null)
            {
                playerEq.BootsId = (BootsObject.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties).Id;
                if (destroyItem) DestroyImmediate(character.GetComponent<HumanController>().Boots);
            }

            playerEquipment.Add(playerEq);
        }

        return playerEquipment;
    }

    public void SetPlayersEq(List<PlayerEquipment> playersEquipment)
    {
        foreach (var playerEq in playersEquipment)
        {
            HumanController humanController = PartyController.instance.CharactersInCamp.Where(x => x.characterProperties.Id == playerEq.Id).First();
            if (playerEq.HelmetId != 0)
            {
                GameObject helmet = ItemFactory.CreateItem(playerEq.HelmetId);
                humanController.Helmet = helmet;
                GameManager.instance.RemoveItemFromEquipment(helmet);
                helmet.transform.SetParent(trash.transform);
            }
            if (playerEq.LeftHandWeaponId != 0)
            {
                GameObject leftHandWeapon = ItemFactory.CreateItem(playerEq.LeftHandWeaponId);
                humanController.LeftHandWeapon = leftHandWeapon;
                GameManager.instance.RemoveItemFromEquipment(leftHandWeapon);
                leftHandWeapon.transform.SetParent(trash.transform);
            }
            if (playerEq.ArmorId != 0)
            {
                GameObject armor = ItemFactory.CreateItem(playerEq.ArmorId);
                humanController.Armor = armor;
                GameManager.instance.RemoveItemFromEquipment(armor);
                armor.transform.SetParent(trash.transform);
            }
            if (playerEq.RightHandWeaponId != 0)
            {
                GameObject rightHandWeapon = ItemFactory.CreateItem(playerEq.RightHandWeaponId);
                humanController.RightHandWeapon = rightHandWeapon;
                GameManager.instance.RemoveItemFromEquipment(rightHandWeapon);
                rightHandWeapon.transform.SetParent(trash.transform);
            }
            if (playerEq.BootsId != 0)
            {
                GameObject boots = ItemFactory.CreateItem(playerEq.BootsId);
                humanController.Boots = boots;
                GameManager.instance.RemoveItemFromEquipment(boots);
                boots.transform.SetParent(trash.transform);
            }

        }
    }
}

[Serializable]
public class Save
{
    public List<PlayerEquipment> PlayersEquipments = new List<PlayerEquipment>();
    public List<HumanSave> HumansSaves = new List<HumanSave>();
    public MapInformation MapInformation;
    public int Traps;
    public List<IntentoryItemSave> InventoryItems = new List<IntentoryItemSave>();
}

[Serializable]
public class HumanSave
{
    public HumanProperties HumanProperties;
    public float PositionX;
    public float PositionY;
}

[Serializable]
public class IntentoryItemSave
{
    public int ItemId;
    public int Count = 1;
}

[Serializable]
public class PlayerEquipment
{
    public string Id;
    public int HelmetId;
    public int ArmorId;
    public int LeftHandWeaponId;
    public int RightHandWeaponId;
    public int BootsId;
}