﻿using UnityEngine;

[ExecuteInEditMode]
public class CharacterOutline : MonoBehaviour
{
    public Color color = Color.white;

    [Range(0, 16)]
    public int outlineSize = 1;

    bool outlineEnabled = false;

    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void DisableOutline()
    {
        outlineEnabled = false;
        UpdateOutline(false);
    }
    public void EnableOutline()
    {
        outlineEnabled = true;
    }

    void OnDisable()
    {
        outlineEnabled = false;
        UpdateOutline(false);
    }

    void Update()
    {
        if (outlineEnabled)
        {
            UpdateOutline(true);
        }
    }

    void UpdateOutline(bool outline)
    {
        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        spriteRenderer.GetPropertyBlock(mpb);
        mpb.SetFloat("_Outline", outline ? 1f : 0);
        mpb.SetColor("_OutlineColor", color);
        mpb.SetFloat("_OutlineSize", outlineSize);
        spriteRenderer.SetPropertyBlock(mpb);
    }
}
