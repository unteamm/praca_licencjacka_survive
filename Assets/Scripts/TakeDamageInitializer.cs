﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageInitializer : MonoBehaviour {

    public void InvokeTakeDamageAnim()
    {
        NightManager.instance.CurrentDefender.SetTakeDamageAnim();
    }

}
