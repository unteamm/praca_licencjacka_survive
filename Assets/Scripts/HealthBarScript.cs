﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    private static float WojtekConstant = 0.564f;
    private float scale = 2f * WojtekConstant;
    public GameObject healthBarCanvas;
    public RectTransform healthBar;
    public RectTransform background;
    private Text text;
    void Awake()
    {

    }

    private void Start()
    {
        text = healthBarCanvas.GetComponentInChildren<Text>();

        SetupBackground();
        UpdateHealth();
    }

    private void SetupBackground()
    {
        background = healthBar.parent.GetComponent<RectTransform>();
        float maxHealth = GetComponent<CharacterController>().characterProperties.MaxHealth;
        background.sizeDelta = new Vector2(maxHealth * scale, background.sizeDelta.y);
    }

    public void ShowBar()
    {
        healthBarCanvas.SetActive(true);
        healthBarCanvas.SetActive(true);
    }

    public void HideBar()
    {
        healthBarCanvas.SetActive(false);
        healthBarCanvas.SetActive(false);
    }

    public void UpdateHealth()
    {
        float health = GetComponent<CharacterController>().GetHealth();
        float maxHealth = GetComponent<CharacterController>().characterProperties.MaxHealth;
        if(health <= 0)
        {
            HideBar();
            return;
        }
        text.text = health + "/" + maxHealth;
        healthBar.sizeDelta = new Vector2(health*scale, healthBar.sizeDelta.y);
    }

}

