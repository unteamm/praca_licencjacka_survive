﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MapInformation {

    public int PlayerCurrentCampId;
    public List<CampNodeProperties> ListaProperties = new List<CampNodeProperties>();

}
