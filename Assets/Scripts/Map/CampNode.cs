﻿using System.Collections.Generic;
using UnityEngine;

public class CampNode : MonoBehaviour
{
    private void Awake()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
    }
    public CampNodeProperties CampProperties;
    public List<CampNode> NextCamps = new List<CampNode>();

    public void OnMouseDown()
    {
        MapManager.instance.SelectedNode = this;
    }
}
