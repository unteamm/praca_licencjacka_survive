﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapGUI : MonoBehaviour
{

    public static MapGUI instance;

    Text locationName;
    Text terrainType;
    Text dangerLevel;
    Button goButton;



    void Awake()
    {
        instance = this;
        locationName = GameObject.Find("LocationTitle").GetComponent<Text>();
        terrainType = GameObject.Find("TypeOfTerrain").GetComponent<Text>();
        dangerLevel = GameObject.Find("DangerLevel").GetComponent<Text>();
        goButton = GameObject.Find("GoToCamp").GetComponent<Button>();
    }

    public void UpdateGui()
    {
        //Refresh camps
        foreach (var node in MapManager.instance.ObozNodes)
        {
            if (node.Equals(MapManager.instance.SelectedNode))
            {
                DrawAsSelected(node);
            }
            else if (node.Equals(MapManager.instance.PlayerNode))
            {
                DrawAsCurrent(node);
            }
            else
            {
                DrawAsNormal(node);
            }
        }
        //Refresh selected camp information
        locationName.text = MapManager.instance.SelectedNode.CampProperties.LocationName;
        terrainType.text = Enum.GetName(typeof(TerrainTypeEnum), MapManager.instance.SelectedNode.CampProperties.TerrainType);
        dangerLevel.text = Enum.GetName(typeof(DangerLevelEnum), MapManager.instance.SelectedNode.CampProperties.DangerLevel);

        switch (MapManager.instance.SelectedNode.CampProperties.DangerLevel)
        {
            case DangerLevelEnum.Low:
                dangerLevel.color = Color.green;
                break;
            case DangerLevelEnum.Moderate:
                dangerLevel.color = Color.blue;
                break;
            case DangerLevelEnum.High:
                dangerLevel.color = Color.yellow;
                break;
            case DangerLevelEnum.VeryHigh:
                dangerLevel.color = Color.Lerp(Color.red, Color.yellow, 0.5f);
                break;
            case DangerLevelEnum.Extreme:
                dangerLevel.color = Color.red;
                break;
            default:
                break;
        }

        if (MapManager.instance.SelectedNode == MapManager.instance.PlayerNode)
        {
            goButton.interactable = false;
        }
        else
            goButton.interactable = true;
    }

    public void DrawAsCurrent(CampNode node)
    {
        GameObject marker = node.transform.Find("Graphics/MapMarker").gameObject;
        marker.GetComponent<SpriteRenderer>().color = Color.gray;
    }

    public void DrawAsSelected(CampNode node)
    {
        GameObject marker = node.transform.Find("Graphics/MapMarker").gameObject;
        marker.GetComponent<SpriteRenderer>().color = Color.black;
    }

    public void DrawAsNormal(CampNode node)
    {
        GameObject marker = node.transform.Find("Graphics/MapMarker").gameObject;
        marker.GetComponent<SpriteRenderer>().color = Color.white;
    }

}
