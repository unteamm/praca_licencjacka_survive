﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour
{
    public Canvas MainCanvas;
    public Material LineRendererMaterial;

    int mapSizeX = 5;
    int mapSizeY = 5;
    float tileHeight = 2f;
    float tileWidth = 2.6f;
    public Transform StartingPosition;

    public static MapManager instance;
    private GameObject campMarkerPrefab;

    private GameObject playerCampObject;
    public GameObject PlayersCampObject
    {
        get
        {
            if (playerCampObject != null)
                return playerCampObject;
            else
            {
                playerCampObject = GameObject.Find("Camp");
                return playerCampObject;
            }
        }
    }

    public List<CampNode> ObozNodes;
    CampNode startingNode;
    CampNode playerNode;
    CampNode selectedNode;

    public MapInformation MapInformation;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        AdjectiveReader.RepopulateList();
        MapInformation = GenerateMapInformation();

        InstantiateFromMapInformation();

        HideMap();

    }

    public CampNode SelectedNode
    {
        get
        {
            return selectedNode;
        }

        set
        {
            if (PlayerNode == value || PlayerNode.NextCamps.Contains(value))
            {
                selectedNode = value;
                MapGUI.instance.UpdateGui();
            }

        }
    }

    public CampNode PlayerNode
    {
        get
        {
            return playerNode;
        }

        set
        {
            playerNode = value;
            MapGUI.instance.UpdateGui();
        }
    }

    public MapInformation GenerateMapInformation()
    {
        System.Random rnd = new System.Random();
        MapInformation mapInformation = new MapInformation();
        CampNodeProperties startingOboz = new CampNodeProperties();
        startingOboz.RandomizeValues();
        startingOboz.TerrainType = TerrainTypeEnum.Forest;
        CampManager.instance.CampProperties = startingOboz;
        startingOboz.SetPositions(0, (mapSizeY / 2));
        mapInformation.ListaProperties.Add(startingOboz);
        mapInformation.PlayerCurrentCampId = startingOboz.Id;

        for (int i = 1; i < mapSizeX; i++)
        {
            int campsCount = Random.Range(1, 4);
            var randomHeight = Enumerable.Range(0, mapSizeY).OrderBy(x => rnd.Next()).Take(campsCount);
            foreach (var height in randomHeight)
            {
                CampNodeProperties campProperties = new CampNodeProperties(i, height);
                campProperties.RandomizeValues();
                mapInformation.ListaProperties.Add(campProperties);
            }
        }
        return mapInformation;
    }

    public void ClearMap()
    {
        ObozNodes.ForEach(x => Destroy(x.gameObject));
        ObozNodes.Clear();
    }

    public void HideMap()
    {
        ObozNodes.ForEach(x => x.gameObject.SetActive(false));
        gameObject.SetActive(false);
        MainCanvas.gameObject.SetActive(true);
        PlayersCampObject.SetActive(true);
        TimeManager.instance.SetNormalSpeed();
    }

    public void ShowMap()
    {
        Camera.main.GetComponent<CameraController>().ResetCamera();
        ObozNodes.ForEach(x => x.gameObject.SetActive(true));
        gameObject.SetActive(true);
        MainCanvas.gameObject.SetActive(false);
        PlayersCampObject.SetActive(false);
        TimeManager.instance.StopTime();
    }

    public void ChangeCamp()
    {
        if (PartyController.instance.characters.FirstOrDefault(x => x.ActualActivity != Activity.idle) != null)
        {
            GUIManager.instance.AppendLogLine("You can't change your camp when your characters are busy!");
            HideMap();
            return;
        }
        if (PartyController.instance.characters.Find(x => x.Exhaustion > 50) == null)
        {
            if (SelectedNode.CampProperties.PosX == mapSizeX - 1)
            {
                SceneManager.LoadScene("THE_END_SCENE");
                return;
            }

            PartyController.instance.characters.ForEach(x => x.Exhaustion+=60);

            PlayerNode = SelectedNode;
            MapInformation.PlayerCurrentCampId = SelectedNode.CampProperties.Id;
            CampManager.instance.CampProperties = SelectedNode.CampProperties;
            CampManager.instance.ReloadCamp();
            CampManager.instance.Traps = 0;
            HideMap();
            TimeManager.instance.InitializeNewDay();

            SaveManager.instance.SaveGame();
        }
        else
        {
            GUIManager.instance.AppendLogLine("Your characters are too exhausted to relocate!");
            HideMap();
        }
    }

    private string GetMarkerPrefabPath(TerrainTypeEnum terrainType)
    {
        string input = "Prefabs/Map/";
        input += terrainType;

        return input;
    }

    private void AddDangerLevelSpriteToObject(GameObject go, DangerLevelEnum danger)
    {
        string spriteId = "1";
        switch (danger)
        {
            case DangerLevelEnum.Low:
                spriteId = "1";
                break;
            case DangerLevelEnum.Moderate:
                spriteId = "2";
                break;
            case DangerLevelEnum.High:
                spriteId = "3";
                break;
            case DangerLevelEnum.VeryHigh:
                spriteId = "4";
                break;
            case DangerLevelEnum.Extreme:
                spriteId = "5";
                break;
            default:
                break;
        }

        GameObject dangerObject = new GameObject("Danger");
        dangerObject.AddComponent<SpriteRenderer>();
        dangerObject.GetComponent<SpriteRenderer>().sprite = Resources.Load("Sprites/Map/" + spriteId, typeof(Sprite)) as Sprite;
        dangerObject.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
        dangerObject.GetComponent<SpriteRenderer>().sortingOrder = 3;
        dangerObject.transform.SetParent(go.transform.Find("Graphics").transform);
        dangerObject.transform.localPosition = new Vector3(0, -0.4f, 0);

    }

    public void InstantiateFromMapInformation()
    {
        ClearMap();
        GameObject MapNodes = transform.FindChild("MapNodes").gameObject;
        MapNodes.transform.parent = transform;
        MapNodes.name = "MapNodes";
        //Adding 1st camp
        var property = MapInformation.ListaProperties[0];
        campMarkerPrefab = Resources.Load(GetMarkerPrefabPath(property.TerrainType), typeof(GameObject)) as GameObject;
        var addedCampNode = Instantiate(campMarkerPrefab, new Vector3(
            property.PosX * tileWidth + StartingPosition.position.x,
            property.PosY * tileHeight + StartingPosition.position.y, 0f), Quaternion.identity).GetComponent<CampNode>();

        AddDangerLevelSpriteToObject(addedCampNode.gameObject, property.DangerLevel);

        addedCampNode.transform.parent = MapNodes.transform;
        addedCampNode.transform.FindChild("Graphics").GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
        addedCampNode.transform.FindChild("Graphics").GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 2;
        addedCampNode.CampProperties = MapInformation.ListaProperties[0];
        ObozNodes.Add(addedCampNode);
        startingNode = addedCampNode;

        for (int i = 1; i < mapSizeX; i++)
        {
            foreach (var camp in MapInformation.ListaProperties.Where(x => x.PosX == i))
            {
                campMarkerPrefab = Resources.Load(GetMarkerPrefabPath(camp.TerrainType), typeof(GameObject)) as GameObject;
                addedCampNode = Instantiate(campMarkerPrefab, new Vector3(
                    camp.PosX * tileWidth + StartingPosition.position.x,
                    camp.PosY * tileHeight + StartingPosition.position.y, 0f), Quaternion.identity).GetComponent<CampNode>();

                AddDangerLevelSpriteToObject(addedCampNode.gameObject, camp.DangerLevel);

                addedCampNode.transform.parent = MapNodes.transform;
                addedCampNode.CampProperties = camp;
                addedCampNode.transform.FindChild("Graphics").GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
                addedCampNode.transform.FindChild("Graphics").GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = 2;
                ObozNodes.Add(addedCampNode);

                //Get random camp to connect with
                var previousCamps = ObozNodes.Where(x => x.CampProperties.PosX == i - 1).ToList();
                var campToConnect = previousCamps[Random.Range(0, previousCamps.Count)];
                campToConnect.NextCamps.Add(addedCampNode);

            }
        }

        //Add connections to camps with dead end
        foreach (var camp in ObozNodes)
        {
            if (camp.NextCamps.Count == 0 && camp.CampProperties.PosX < mapSizeX - 1)
            {
                var availableCamps = ObozNodes.Where(x => x.CampProperties.PosX == camp.CampProperties.PosX + 1).ToList();
                var nextCamp = availableCamps[Random.Range(0, availableCamps.Count)];
                camp.NextCamps.Add(nextCamp);
            }
        }
        playerNode = selectedNode = ObozNodes.Where(x => x.CampProperties.Id == MapInformation.PlayerCurrentCampId).First();
        RenderConnections();
        MapGUI.instance.UpdateGui();
    }

    public void RenderConnections()
    {
        foreach (var node in ObozNodes.Take(ObozNodes.Count - 1))
        {
            var nextCamps = node.GetComponent<CampNode>().NextCamps;
            foreach (var camp in nextCamps)
            {
                var lineRenderer = new GameObject("LineRenderer").AddComponent<LineRenderer>();
                lineRenderer.gameObject.transform.parent = node.transform;
                lineRenderer.material = LineRendererMaterial;
                lineRenderer.widthMultiplier = 0.1f;
                lineRenderer.SetPositions(new Vector3[] { node.transform.position, camp.transform.position });
                lineRenderer.sortingLayerName = "Foreground";
                lineRenderer.sortingOrder = 2;
            }
        }
    }
}
