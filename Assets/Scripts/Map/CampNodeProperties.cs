﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public enum DangerLevelEnum
{
    Low,
    Moderate,
    High,
    VeryHigh,
    Extreme,
}

public enum TerrainTypeEnum
{
    City,
    Forest,
    Scrapyard,
    Lake,
    //Sea,
    Village,
    Barn,
}

[Serializable]
public class CampNodeProperties
{


    private static int idCounter = 0;

    public int Id;
    public int PosX;
    public int PosY;
    public DangerLevelEnum DangerLevel;
    public string LocationName;
    public TerrainTypeEnum TerrainType;

    public CampNodeProperties()
    {
        Id = idCounter++;
    }

    public CampNodeProperties(int x, int y) : this()
    {
        PosX = x;
        PosY = y;
    }

    public void RandomizeValues()
    {
        DangerLevel = (DangerLevelEnum)UnityEngine.Random.Range(0, Enum.GetNames(typeof(DangerLevelEnum)).Length);
        TerrainType = (TerrainTypeEnum)UnityEngine.Random.Range(0, Enum.GetNames(typeof(TerrainTypeEnum)).Length);

        LocationName =  AdjectiveReader.GetRandomAdjective() + " " + TerrainType.ToString();
    }

    public void SetPositions(int x, int y)
    {
        PosX = x;
        PosY = y;
    }
}
