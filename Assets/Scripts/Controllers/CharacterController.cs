﻿using Assets.Scripts.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class CharacterController : MonoBehaviour, ISelectable, IDamageable
{
    public AudioClip HitSound;
    public AudioClip GetHitSound;
    public AudioClip DieSound;
    public AudioClip WalkSound;
    public AudioClip RustleSound;

    AudioSource audioSource;

    public LayerMask SortingOrderLayerMask;
    public HumanProperties characterProperties;
    public GameObject ProgressBar;
    protected Animator anim;
    GameObject playerGraphics;
    protected int _speed = 10;
    protected Vector3 _destPosition;
    protected Vector3 _defaultPosition;
    Queue<Node> path;
    IEnumerator FollowPathIEnumerator;
    private float starTime;
    public SpriteRenderer sprite;
    private Transform headTrigger;
    private Transform legsTrigger;

    public Vector3 DefaultPosition
    {
        get
        {
            return _defaultPosition;
        }
        set
        {
            _defaultPosition = value;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        CalculateSortingOrder();
    }

    protected IEnumerator IdleCoroutine()
    {
        while (true)
        {
            if (IsBusy)
            {
                yield return new WaitForSeconds(4f);
            }
            else
            {
                anim.SetTrigger("SetIdleInterrupt");
                yield return new WaitForSeconds(6f);
            }
        }
    }

    public bool IsBusy
    {
        get
        {
            if (characterProperties.ActualActivity == Activity.idle)
            {
                return false;
            }
            else
            {
                GUIManager.instance.AppendLogLine(characterProperties.FirstName + " is busy.");
                return true;
            }
        }
    }

    bool targetArrived = true;

    public void Awake()
    {
        playerGraphics = transform.FindChild("Graphics").gameObject;
        //_defaultPosition = transform.position;
        _defaultPosition = Pathfinding.instance.GetClosestWalkablePosition(transform.position);
        transform.position = _defaultPosition;
        _destPosition = transform.position;
        anim = GetComponentInChildren<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        headTrigger = transform.FindChild("HeadTrigger");
        legsTrigger = transform.FindChild("LegsTrigger");
        audioSource = GetComponent<AudioSource>();
    }

    SpriteRenderer healthBarSprite
    {
        get
        {
            Transform trans = transform.FindChild("HealthBar");
            if (trans != null)
            {
                return trans.GetComponent<SpriteRenderer>();
            }
            return null;
        }
    }

    protected virtual void Start()
    {
        //  StartCoroutine(IdleCoroutine());
        CalculateSortingOrder();
        //   sprite.sortingOrder = (int)Camera.main.WorldToScreenPoint(sprite.bounds.min).y * -1;

    }

    public void CalculateSortingOrder()
    {
        if (headTrigger == null || legsTrigger == null)
        {
            return;
        }

        bool headCollides = false;
        bool legsCollides = false;
        int sortingOrder = 1;
        Collider2D[] hitHeadColliders = Physics2D.OverlapCircleAll(headTrigger.position, .3f, SortingOrderLayerMask);
        int i = 0;
        while (i < hitHeadColliders.Length)
        {
            if (hitHeadColliders[i].transform.root != transform)
            {
                if (hitHeadColliders[i].GetComponentInChildren<SpriteRenderer>() != null)
                {
                    headCollides = true;
                    sortingOrder = hitHeadColliders[i].GetComponentInChildren<SpriteRenderer>().sortingOrder;
                }
            }
            i++;
        }
        Collider2D[] hitLegsColliders = Physics2D.OverlapCircleAll(legsTrigger.position, .3f, SortingOrderLayerMask);
        int j = 0;
        while (j < hitLegsColliders.Length)
        {
            if (hitLegsColliders[j].transform.root != transform)
            {
                if (hitLegsColliders[j].GetComponentInChildren<SpriteRenderer>() != null)
                {
                    legsCollides = true;
                }
            }
            j++;
        }

        if (headCollides == true && legsCollides == false)
        {
            sprite.sortingOrder = sortingOrder + 1;
            if (healthBarSprite != null)
                healthBarSprite.sortingOrder = sortingOrder + 1;
        }
        else if (headCollides && legsCollides)
        {
            sprite.sortingOrder = 0;
            if (healthBarSprite != null)
                healthBarSprite.sortingOrder = 0;
        }
        else
        {
            sprite.sortingOrder = 0;
            if (healthBarSprite != null)
                healthBarSprite.sortingOrder = 0;
        }
    }

    public void SetActivityAsIdle()
    {
        characterProperties.ActualActivity = Activity.idle;
    }

    public void SetActivity(Activity _activity)
    {
        characterProperties.ActualActivity = _activity;
    }

    public string GetActualActivity()
    {
        return characterProperties.FirstName + " is " + characterProperties.ActualActivity.ToString();
    }

    public bool CheckIfTargetArrived()
    {
        return targetArrived;
    }

    public void GoTo(Vector3 destPoint)
    {
        targetArrived = false;
        path = Pathfinding.instance.FindPath(transform.position, destPoint);
        FollowPathIEnumerator = FollowPath();
        StartCoroutine(FollowPathIEnumerator);
    }

    IEnumerator FollowPath()
    {
        Play(WalkSound, true);
        Node currentNode = path.Dequeue();
        while (true)
        {
            if (transform.position == currentNode.WorldPosition)
            {
                if (path.Count == 0)
                {
                    targetArrived = true;
                    Stop();
                    anim.ResetTrigger("GoLeft");
                    anim.ResetTrigger("GoRight");
                    anim.SetTrigger("SetIdle");
                    yield break;
                }
                currentNode = path.Dequeue();
            }
            if ((currentNode.WorldPosition - transform.position).normalized.x > 0)
            {
                anim.SetTrigger("GoRight");
            }
            else if ((currentNode.WorldPosition - transform.position).normalized.x < 0)
            {
                anim.SetTrigger("GoLeft");
            }

            transform.position = Vector3.MoveTowards(transform.position, currentNode.WorldPosition, _speed * Time.deltaTime);
            //sprite.sortingOrder = -Mathf.RoundToInt(transform.position.y);
            //sprite.sortingOrder = (int)Camera.main.WorldToScreenPoint(sprite.bounds.min).y * -1;
            CalculateSortingOrder();
            yield return null;

        }
    }


    public void GoToWithoutPathfinding(Vector3 destPoint)
    {
        _destPosition = destPoint;
    }

    public void ReturnToDefaultPosition()
    {
        if (Pathfinding.instance.CheckIfVectorsAreOnTheSameTile(transform.position, _defaultPosition))
        {
            return;
        }
        targetArrived = false;
        path = Pathfinding.instance.FindPath(transform.position, _defaultPosition);
        FollowPathIEnumerator = FollowPath();
        StartCoroutine(FollowPathIEnumerator);
    }

    public IEnumerator ReturnToDefaultPositionEnumerator()
    {
        if (Pathfinding.instance.CheckIfVectorsAreOnTheSameTile(transform.position, _defaultPosition))
        {
            yield break;
        }
        targetArrived = false;
        path = Pathfinding.instance.FindPath(transform.position, _defaultPosition);
        if (FollowPathIEnumerator != null)
            StopCoroutine(FollowPathIEnumerator);
        yield return StartCoroutine(FollowPath());
    }

    public HumanProperties GetCharacterProperties()
    {
        return characterProperties;
    }

    public void HideCharacterGraphics()
    {
        playerGraphics.SetActive(false);
    }

    public void ShowCharacterGraphics()
    {
        playerGraphics.SetActive(true);
    }

    public virtual void SetCharacterProperties(HumanProperties _characterProperties)
    {
        characterProperties = _characterProperties;
    }

    public void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            GameManager.instance.SetSelectedCharacter(this);
        }
    }

    public void OnMouseOver()
    {
        if (GameManager.instance.InputPossibleCheck() && characterProperties.ActualActivity == Activity.idle)
        {
            SetIdleInterruptAnim();
        }
    }

    public void AttackCharacter(CharacterController defender)
    {
        anim.SetTrigger("Attack");
        PlayOnce(HitSound);


        string debug = GetCharacterProperties().FirstName + " atakuje ";
        debug += defender.GetCharacterProperties().FirstName + " ktory ma: ";
        debug += defender.GetCharacterProperties().Health + " zdrowia i zadaje mu: ";
        debug += GetCharacterProperties().FightingSkill + " obrażeń";

        defender.ReceiveDamage(GetCharacterProperties().FightingSkill);
        GUIManager.instance.AppendLogLine(debug);
        //    Debug.Log(debug);
    }

    public void UpdateProgressBar(float progress)
    {
        ProgressBar.SetActive(true);
        var progressLine = ProgressBar.transform.GetChild(0);
        progressLine.localScale = new Vector3(progress / 100, 1, 1);
        if (Math.Abs(progress - 100) < 0.0001)
            ProgressBar.SetActive(false);
    }

    public IEnumerator UpdateProgressBarToNight()
    {
        ProgressBar.SetActive(true);
        starTime = (float)TimeManager.instance.GetTime().TimeOfDay.TotalSeconds;
        while (TimeManager.instance.GetTime().TimeOfDay.TotalSeconds <= 79199)
        {
            var progressLine = ProgressBar.transform.GetChild(0);
            float progress = (float)(TimeManager.instance.GetTime().TimeOfDay.TotalSeconds - starTime) / (79200 - starTime);
            progressLine.localScale = new Vector3(progress, 1, 1);
            yield return new WaitForSeconds(0.1f);
        }
        ProgressBar.SetActive(false);
    }

    public virtual void ReceiveDamage(float damage)
    {
        PlayOnce(GetHitSound);

        if (GetHealth() - damage <= 0)
            SetHealth(0);
        else
            SetHealth(GetHealth() - damage);

        Die();
    }

    public virtual void Die()
    {
        if (GetHealth() <= 0)
        {
            if (this is HumanController)
            {
                PartyController.instance.RemoveHumanControllerFromAllLists(this as HumanController);
                AvatarPanelController.Instance.RemoveAvatar((HumanController)this);
                PartyController.instance.CharactersInstances.Remove(this.gameObject);
            }
            PlayOnce(DieSound);
            anim.SetTrigger("Die");
            Destroy(this.gameObject, 3f);
        }
    }

    public void SetHealth(float health)
    {
        characterProperties.Health = health;
        if (characterProperties.Health > characterProperties.MaxHealth)
            characterProperties.Health = characterProperties.MaxHealth;

        GetComponent<HealthBarScript>().UpdateHealth();
    }

    public float GetHealth()
    {
        return characterProperties.Health;
    }

    public void SetTakeDamageAnim()
    {
        anim.SetTrigger("TakeDamage");
    }

    public void SetIdleAnim()
    {
        anim.SetTrigger("SetIdle");
    }

    public void SetIdleInterruptAnim()
    {
        anim.SetTrigger("SetIdleInterrupt");
    }

    public void ShowHealthBar()
    {
        GetComponent<HealthBarScript>().ShowBar();
    }

    public void HideHealthBar()
    {
        GetComponent<HealthBarScript>().HideBar();
    }


    public void PlayOnce(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public void Play(AudioClip clip, bool _loop = false)
    {
        if (_loop)
        {
            audioSource.loop = true;
        }
        else
        {
            audioSource.loop = false;
        }
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void Stop()
    {
        audioSource.clip = null;
        audioSource.Stop();
    }
}
