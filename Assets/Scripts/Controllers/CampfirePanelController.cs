﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfirePanelController : MonoBehaviour {

    public void OnIgniteButtonClick()
    {
        if (!CampfireController.instance.IsFire)
        {
            if (CampfireController.instance.campfireSlot.transform.childCount == 0)
            {
                GUIManager.instance.AppendLogLine("You need a wood.");
                return;
            }
            CampfireController.instance.campfirePanel.SetActive(false);
            CampfireController.instance.startIgniteIenumerator();
        }
        else
            GUIManager.instance.AppendLogLine("The campfire is burning");
    }

    public void OnExitButtonClick()
    {
        CampfireController.instance.campfirePanel.SetActive(false);
    }

}
