﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentController : InteractableObjectController
{
    private int progress;

    public override void Awake()
    {
        base.Awake();
        panelDetails = new InteractPanelDetails();
        panelDetails.Header = "Tent";

        ButtonDetails sleepBtnDetails = new ButtonDetails();
        sleepBtnDetails.buttonTitle = "Sleep";
        sleepBtnDetails.action = startSleepIenumerator;

        ButtonDetails trapBtnDetails = new ButtonDetails();
        trapBtnDetails.buttonTitle = "Build Trap";
        trapBtnDetails.action = startBuildingTrap;


        panelDetails.button1Details = sleepBtnDetails;
        panelDetails.button2Details = trapBtnDetails;
    }

    public override void OnMouseDown()
    {
        if (GameManager.instance.InputPossibleCheck() && !GameManager.instance.GetSelectedCharacter().IsBusy)
        {
            GUIManager.instance.ShowInteractPanel(panelDetails);
        }
    }

    void startSleepIenumerator()
    {
        HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
        characterController.CurrentCoroutine =  Sleep(characterController);
        characterController.StartCurrentCoroutine();
    }

    IEnumerator Sleep(HumanController characterController)
    {
        GameManager.instance.SetSelectedCharacter(null);
        characterController.SetActivity(Activity.sleeping);
        Vector3 startPosition = characterController.transform.position;
        characterController.GoTo(interactPlace);
        yield return new WaitForSeconds(2f);
        StartCoroutine(characterController.UpdateProgressBarToNight());
        characterController.HideCharacterGraphics();
        characterController.GetCharacterProperties().Exhaustion -= 50;
    }

    void startBuildingTrap()
    {
        HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
        if (PartyController.instance.IsAnyoneBuildingTrap)
            return;
        if (CanPerformAction(characterController))
        {
            GameManager.instance.SetSelectedCharacter(null);
            characterController.CurrentCoroutine = BuildTrap(characterController);
            characterController.StartCurrentCoroutine();
        }
    }

    IEnumerator BuildTrap(HumanController characterController)
    {
        progress = 0;
        characterController.SetActivity(Activity.building);
        Vector3 startPosition = characterController.transform.position;
        characterController.GoTo(interactPlace);
        while (!characterController.CheckIfTargetArrived())
        {
            yield return new WaitForSeconds(0.1f);
        }
        characterController.SetAnimTrigger("DoSmth");
        characterController.Play(characterController.RustleSound);
        while (progress < 100)
        {
            progress += 2;
            characterController.UpdateProgressBar(progress);
            yield return new WaitForSeconds(0.1f);
        }
        characterController.SetAnimTrigger("EndDoSmth");
        characterController.Stop();
        yield return new WaitForSeconds(1f);
        CampManager.instance.Traps++;
        characterController.ReturnToDefaultPosition();
        while (!characterController.CheckIfTargetArrived())
        {
            yield return new WaitForSeconds(0.1f);
        }
        characterController.SetActivityAsIdle();
    }
}
