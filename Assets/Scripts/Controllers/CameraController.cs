﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public int Boundary = 50;
    public int speed = 5;

    private int screenWidth;
    private int screenHeight;
    private float Border = 5;
	// Use this for initialization
	void Start () {
        screenHeight = Screen.height;
        screenWidth = Screen.width;

        if (Camera.main.aspect >= 1.7)
        {
            Border = 5f;
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Border = 7f;
        }
        else
        {
            Border = 8.5f;
        }

    }


    public void ResetCamera()
    {
        this.gameObject.transform.position = new Vector3(0f, 0f, -10f);
    }

    // Update is called once per frame
    void Update () {
		if(Input.mousePosition.x > screenWidth - Boundary && transform.position.x < Border)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.mousePosition.x < 0 + Boundary && transform.position.x > -Border)
        {
            transform.position += -Vector3.right * speed * Time.deltaTime;
        }
    }
}
