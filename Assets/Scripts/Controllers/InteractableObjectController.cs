﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObjectController : MonoBehaviour, ISelectable {
    protected InteractPanelDetails panelDetails;
    public CharacterOutline outlineScript;
    protected Vector3 interactPlace;
    public AudioSource audioSource;

    public virtual void OnMouseDown()
    {
        throw new NotImplementedException();
    }

    public virtual void OnMouseOver()
    {
        if (GameManager.instance.GetSelectedCharacter() != null)
        {
            outlineScript.EnableOutline();
        }
    }

    private void OnMouseExit()
    {
        outlineScript.DisableOutline();
    }

    protected bool CanPerformAction()
    {
        if (GameManager.instance.GetSelectedCharacter().GetCharacterProperties().Exhaustion < 60)
        {
            GameManager.instance.GetSelectedCharacter().GetCharacterProperties().Exhaustion+= 40;
            return true;
        }
        else
        {
            GUIManager.instance.AppendLogLine(GameManager.instance.GetSelectedCharacter().name + " is too tired!");
            return false;
        }
    }

    protected bool CanPerformAction(HumanController humanController)
    {
        if (humanController.GetCharacterProperties().Exhaustion < 60)
        {
            humanController.GetCharacterProperties().Exhaustion+=40;
            return true;
        }
        else
        {
            GUIManager.instance.AppendLogLine(humanController.name + " is too tired!");
            return false;
        }
    }

    public virtual void Awake()
    {
        outlineScript = GetComponentInChildren<CharacterOutline>();
        audioSource = GetComponent<AudioSource>();
        if (transform.FindChild("InteractPlace") != null)
        {
            interactPlace = transform.FindChild("InteractPlace").transform.position;
        }
        else
        {
            interactPlace = transform.position;
        }
    }

    public void PlayOnce(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public void Play(AudioClip clip, bool _loop = false)
    {
        if (_loop)
        {
            audioSource.loop = true;
        }
        else
        {
            audioSource.loop = false;
        }
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void Stop()
    {
        audioSource.clip = null;
        audioSource.Stop();
    }


}
