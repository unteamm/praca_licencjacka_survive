﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AvatarPanelController : MonoBehaviour
{
    public static AvatarPanelController Instance;

    public GameObject AvatarPrefab;
    public GameObject AvatarPanel;
    public GameObject EquipmentPanel;
    private List<GameObject> avatarList;

    void Awake()
    {
        Instance = this;
        avatarList = new List<GameObject>();
    }

    public void AddAvatar(HumanController character)
    {
        GameObject avatar = Instantiate(AvatarPrefab);
        avatar.GetComponent<Image>().sprite = Resources.Load<Sprite>(character.characterProperties.Avatar);
        avatar.GetComponent<AvatarPrefabController>().HumanController = character;
        avatar.transform.SetParent(AvatarPanel.transform, false);
        avatar.name = character.characterProperties.LastName + "_Avatar";
        character.ProgressBar = avatar.transform.GetChild(0).gameObject;
        avatarList.Add(avatar);
    }

    public void RemoveAvatar(HumanController character)
    {
        string searchedString = character.characterProperties.LastName + "_Avatar";
        GameObject goToDestroy = avatarList[0];
        foreach (GameObject go in avatarList)
        {
            if (go.name == searchedString)
            {
                goToDestroy = go;
                break;
            }
        }
        avatarList.Remove(goToDestroy);
        DestroyImmediate(goToDestroy);
    }

    public void DisableAvatar(HumanController character)
    {
        string searchedString = character.characterProperties.LastName + "_Avatar";
        foreach(GameObject go in avatarList)
        {
            if(go.name == searchedString)
            {
                go.GetComponent<Button>().interactable = false;
                return;
            }
        }
    }

    public void EnableAvatar(HumanController character)
    {
        string searchedString = character.characterProperties.LastName + "_Avatar";
        foreach (GameObject go in avatarList)
        {
            if (go.name == searchedString)
            {
                go.GetComponent<Button>().interactable = true;
                return;
            }
        }
    }

    public void RemoveAllAvatars()
    {
        foreach (var avatar in avatarList)
        {
            Destroy(avatar);
        }
        avatarList.Clear();
    }

    public void CloseOnClick()
    {
        EquipmentPanel.SetActive(false);
        GUIManager.instance.HideCharacterPanel();
    }

    
}
