﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PartyController : MonoBehaviour
{
    public static PartyController instance;

    public Dictionary<string, GameObject> charactersPrefabs;
    public List<HumanProperties> characters;
    public List<GameObject> CharactersInstances;
    public List<HumanController> CharactersInCamp
    {
        get
        {
            var list = GameObject.FindGameObjectsWithTag("Human");
            var controllerList = list.Select(x => x.GetComponent<HumanController>()).ToList();
            return controllerList;
        }
    }

    private List<PlayerEquipment> playersEquipmentsIds;

    void Awake()
    {
        instance = this;
        charactersPrefabs = new Dictionary<string, GameObject>();
        foreach (GameObject go in Resources.LoadAll(("Prefabs/Characters"), typeof(GameObject)))
        {
            charactersPrefabs.Add(go.GetComponent<CharacterController>().characterProperties.Id, go);
        }
        characters = new List<HumanProperties>();
        playersEquipmentsIds = new List<PlayerEquipment>();
    }

    public void RemoveHumanControllerFromAllLists(HumanController controller)
    {
        if (characters.Contains(controller.GetCharacterProperties()))
            characters.Remove(controller.GetCharacterProperties());

    }

    public void addToParty(HumanProperties _character)
    {
        characters.Add(_character);
    }

    public void SpawnAllCharacters(bool areChangingCamp = true)
    {
        CharactersInstances = new List<GameObject>();
        foreach (HumanProperties character in characters)
        {
            GameObject characterGameObject = Instantiate(charactersPrefabs[character.Id], CampManager.instance.GetFreeSpawnPoint().position, Quaternion.identity) as GameObject;
            AvatarPanelController.Instance.AddAvatar(characterGameObject.GetComponent<HumanController>());
            characterGameObject.GetComponent<HumanController>().SetCharacterProperties(character);
            characterGameObject.name = charactersPrefabs[character.Id].name;
            characterGameObject.GetComponent<HumanController>().SetActivityAsIdle();
            CharactersInstances.Add(characterGameObject);
        }
        if (areChangingCamp && playersEquipmentsIds.Count != 0)
        {
            SaveManager.instance.SetPlayersEq(playersEquipmentsIds);
        }

    }

    public void DespawnAllCharacters()
    {
        playersEquipmentsIds.Clear();
        playersEquipmentsIds = SaveManager.instance.GetPlayersEquipmentsIds(true);
        foreach (var character in CharactersInstances)
        {
            DestroyImmediate(character);
        }
        AvatarPanelController.Instance.RemoveAllAvatars();
        CharactersInstances.Clear();
    }

    public void GenerateRandomCharacters(int number)
    {
        if (number > 5) number = 5;
        if (number <= 0) number = 1;

        List<string> availableIds = charactersPrefabs.Keys.ToList();

        characters = new List<HumanProperties>();
        for (int i = 0; i < number; i++)
        {
            string id = availableIds[Random.Range(0, availableIds.Count)];
            availableIds.Remove(id);
            HumanProperties defaultProp = charactersPrefabs[id].GetComponent<CharacterController>().characterProperties;
            HumanProperties properties = new HumanProperties(defaultProp.FirstName, defaultProp.LastName, defaultProp.Age, defaultProp.Health, defaultProp.IsHungry, defaultProp.Morale, defaultProp.ExplorationSkill, 0f, defaultProp.HungerDay, defaultProp.FightingSkill, defaultProp.Avatar, defaultProp.Id);
            addToParty(properties);
        }
    }

    public bool IsAnyoneBuildingTrap
    {
        get { return CharactersInstances.Find(x => x.GetComponent<CharacterController>().characterProperties.ActualActivity == Activity.building) != null; }
    }
    public bool IsAnyoneIgniting
    {
        get { return CharactersInstances.Find(x => x.GetComponent<CharacterController>().characterProperties.ActualActivity == Activity.igniting) != null; }
    }
    public bool IsAnyoneBaking
    {
        get { return CharactersInstances.Find(x => x.GetComponent<CharacterController>().characterProperties.ActualActivity == Activity.baking) != null; }
    }
}
