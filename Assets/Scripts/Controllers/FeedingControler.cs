﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedingControler : MonoBehaviour
{
    public static FeedingControler instance;

    public GameObject Feeding;
    public GameObject FeedingSlots;
    public GameObject FeedSlot;
    private bool _visibility = false;
    private int _moraleToSubstract = 1;
    
	void Start ()
	{
	    instance = this;
	    Feeding.SetActive(false);
	}

    public void Feed()
    {
        List<HumanProperties> humanList = new List<HumanProperties>(PartyController.instance.characters);

        for (int i = 0; i < FeedingSlots.transform.childCount; i++)
        {
            GameObject food = null;
            var humanProperties = FeedingSlots.transform.GetChild(i).GetComponentInChildren<Image>().GetComponent<CharacterContainer>().Character;
            if (FeedingSlots.transform.GetChild(i).GetComponentInChildren<Image>().transform.childCount == 0) continue;
            food = FeedingSlots.transform.GetChild(i).GetComponentInChildren<Image>().transform.GetChild(0).transform.gameObject;

            humanList.Remove(humanProperties);
            humanProperties.IsHungry = false;
            humanProperties.HungerDay = 0;
            GameManager.instance.RemoveItemFromEquipment(food);
            Destroy(food);
        }

        foreach (var human in humanList)
        {
            human.HungerDay++;
            human.IsHungry = true;
            if (human.HungerDay >= 2)
                human.Morale -= _moraleToSubstract;
        }
        TimeManager.instance.ShowTimePanel();
        Hide();
    }

    public void Show()
    {
        TimeManager.instance.HideTimePanel();
        Clear();
        EquipmentController.instance.Show();
        Feeding.SetActive(true);
        List<HumanProperties> characters = PartyController.instance.characters;
        List<HumanController> scavengers = GameManager.instance.Scavengers;
        foreach (var character in characters)
        {
            if (CheckDouble(scavengers, character)) continue;
            GameObject feedSlot = Instantiate(FeedSlot);
            feedSlot.GetComponentInChildren<Text>().text = character.FirstName + " " + character.LastName;
            feedSlot.transform.SetParent(FeedingSlots.transform,false);
            feedSlot.GetComponentInChildren<Image>().GetComponent<CharacterContainer>().Character = character;
        }
        TimeManager.instance.StopTime();
    }

    private void Clear()
    {
        for (int i = 0; i < FeedingSlots.transform.childCount; i++)
        {
            Destroy(FeedingSlots.transform.GetChild(i).transform.gameObject);
        }
    }

    private bool CheckDouble(List<HumanController> humanControllers, CharacterProperties character)
    {
        foreach (var human in humanControllers)
        {
            if (human.GetCharacterProperties() == character)
                return true;
        }
        return false;
    }

    public void Hide()
    {
        EquipmentController.instance.Hide();
        Feeding.SetActive(false);
    }
}
