﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitController : InteractableObjectController {


    void Start()
    {
        panelDetails = new InteractPanelDetails();
        panelDetails.Header = "Exit";
        ButtonDetails bakeBtnDetails = new ButtonDetails();
        bakeBtnDetails.buttonTitle = "Leave Camp";
        bakeBtnDetails.action = MapManager.instance.ShowMap;
        panelDetails.button1Details = bakeBtnDetails;
        ButtonDetails bakeBtn2Details = new ButtonDetails();
        bakeBtn2Details.buttonTitle = "Look for resources";
        bakeBtn2Details.action = startBeginScavengingIenumerator;
        panelDetails.button2Details = bakeBtn2Details;
    }

    public override void OnMouseDown()
    {
        //base.OnMouseDown();
        if (GameManager.instance.InputPossibleCheck() && !GameManager.instance.GetSelectedCharacter().IsBusy)
        {
            GUIManager.instance.ShowInteractPanel(panelDetails);
        }
    }

    void startBeginScavengingIenumerator()
    {
        HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
        characterController.CurrentCoroutine = BeginScavenging(characterController);
        characterController.StartCurrentCoroutine();
    }

    public void StartContinueScavenging(HumanController characterController)
    {
        characterController.CurrentCoroutine = ContinueScavenging(characterController);
        characterController.StartCurrentCoroutine();
    }


    IEnumerator BeginScavenging(HumanController characterController)
    {
        if (!CanPerformAction())
        {
            yield break;
        }
        GameManager.instance.SetSelectedCharacter(null);
        characterController.SetActivity(Activity.scavenging);
        characterController.GoTo(interactPlace);
        yield return new WaitForSeconds(3f);
        StartCoroutine(characterController.UpdateProgressBarToNight());
        GameManager.instance.AddScavenger((HumanController)characterController);
    }

    IEnumerator ContinueScavenging(HumanController characterController)
    {
        GameManager.instance.SetSelectedCharacter(null);
        characterController.SetActivity(Activity.scavenging);
        characterController.GoTo(interactPlace);
        yield return new WaitForSeconds(3f);
        StartCoroutine(characterController.UpdateProgressBarToNight());
        GameManager.instance.AddScavenger((HumanController)characterController);
    }
}
