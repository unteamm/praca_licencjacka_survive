﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanController : CharacterController
{
    public IEnumerator CurrentCoroutine;

    List<int> collectedItems = new List<int>();

    public GameObject Helmet;
    public GameObject Armor;
    public GameObject LeftHandWeapon;
    public GameObject RightHandWeapon;
    public GameObject Boots;
    public CharacterOutline outlineScript;

    [HideInInspector]
    public bool WasScavengingInterrupted = false;

    public void SetAnimTrigger(string _triggerName)
    {
        anim.SetTrigger(_triggerName);
    }

    protected override void Start()
    {
        base.Start();
        HideHealthBar();
        outlineScript = GetComponentInChildren<CharacterOutline>();
        outlineScript.DisableOutline();
    }
    public void StartCurrentCoroutine()
    {
        StartCoroutine(CurrentCoroutine);
    }

    public IEnumerator StopAndGoBackToCamp()
    {
        if (CurrentCoroutine != null)
            StopCoroutine(CurrentCoroutine);

        ProgressBar.SetActive(false);
        ReturnCollectedItems();
        ShowCharacterGraphics();
        AvatarPanelController.Instance.EnableAvatar(this);
        yield return StartCoroutine(ReturnToDefaultPositionEnumerator());
    }

    public void AddItemIdToCollected(int id)
    {
        collectedItems.Add(id);
    }
    public void ReturnCollectedItems()
    {
        foreach (var item in collectedItems)
        {
            ItemFactory.CreateItem(item);
        }
        collectedItems = new List<int>();
    }

    //Character Equipment
    public void AddToCharacterEquipment(GameObject item, bool isLeftHand = false)
    {
        var itemProperties = item.GetComponent<ItemController>().ItemProperties as WearableItemProperties;
        if (itemProperties == null) return;
        switch (itemProperties.Type)
        {
            case ItemProperties.ItemType.Armor:
                Armor = item;
                break;
            case ItemProperties.ItemType.Boots:
                Boots = item;
                break;
            case ItemProperties.ItemType.Helmet:
                Helmet = item;
                break;
            case ItemProperties.ItemType.Weapon:
                if (isLeftHand) LeftHandWeapon = item;
                else RightHandWeapon = item;
                break;
        }

        AddStats(itemProperties);
        GameManager.instance.RemoveItemFromEquipment(item);
    }

    public void RemoveFromCharacterEquipment(GameObject item, bool isLeftHand = false)
    {
        var itemProperties = item.GetComponent<ItemController>().ItemProperties as WearableItemProperties;
        if (itemProperties == null) return;
        switch (itemProperties.Type)
        {
            case ItemProperties.ItemType.Armor:
                Armor = null;
                break;
            case ItemProperties.ItemType.Boots:
                Boots = null;
                break;
            case ItemProperties.ItemType.Helmet:
                Helmet = null;
                break;
            case ItemProperties.ItemType.Weapon:
                if (isLeftHand) LeftHandWeapon = null;
                else RightHandWeapon = null;
                break;
        }

        RemoveStats(itemProperties);
        GameManager.instance.AddItemToEquipment(item);
    }

    private void AddStats(WearableItemProperties wearableItem)
    {
        GetCharacterProperties().FightingSkill += wearableItem.FightingToAdd;
        SetHealth(GetHealth() + wearableItem.HealthToAdd);
        GetCharacterProperties().ExplorationSkill += wearableItem.ExplorationToAdd;
        GetCharacterProperties().Morale += wearableItem.MoraleToAdd;
        GetCharacterProperties().Exhaustion -= wearableItem.ActionPointsToAdd;
        GUIManager.instance.UpdateCharacterPanel(GetCharacterProperties());
    }

    private void RemoveStats(WearableItemProperties wearableItem)
    {
        GetCharacterProperties().FightingSkill -= wearableItem.FightingToAdd;
        SetHealth(GetHealth() - wearableItem.HealthToAdd);
        GetCharacterProperties().ExplorationSkill -= wearableItem.ExplorationToAdd;
        GetCharacterProperties().Morale -= wearableItem.MoraleToAdd;
        GetCharacterProperties().Exhaustion += wearableItem.ActionPointsToAdd;
        GUIManager.instance.UpdateCharacterPanel(GetCharacterProperties());
    }
}
