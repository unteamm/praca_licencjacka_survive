﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CampfireController : InteractableObjectController
{
    public static CampfireController instance;
    public GameObject campfirePanel;
    public GameObject campfireSlot;
    public Text campfireTime;
    private int progress;
    private Animator animator;
    public bool IsFire;
    public AudioClip fireClip;

    public override void Awake()
    {
        base.Awake();
        panelDetails = new InteractPanelDetails();
        panelDetails.Header = "Campfire";
        ButtonDetails igniteBtnDetails = new ButtonDetails();
        igniteBtnDetails.buttonTitle = "Ignite";
        igniteBtnDetails.action = ShowIgnitePanel;
        panelDetails.button1Details = igniteBtnDetails;
        ButtonDetails bakeBtnDetails = new ButtonDetails();
        bakeBtnDetails.buttonTitle = "Bake";
        bakeBtnDetails.action = startBakeIenumerator;
        panelDetails.button2Details = bakeBtnDetails;

        IsFire = false;
        instance = this;
    }

    void Start()
    {
        campfirePanel = GameObject.Find("Canvas").transform.Find("CampfirePanel").gameObject;
        campfireSlot = campfirePanel.transform.Find("CampfireSlot").gameObject;
        campfireTime = campfirePanel.transform.Find("Time").gameObject.GetComponent<Text>();
        animator = transform.GetComponentInChildren<Animator>();
    }

    public override void OnMouseDown()
    {
        if (GameManager.instance.InputPossibleCheck() && !GameManager.instance.GetSelectedCharacter().IsBusy)
        {
            GUIManager.instance.ShowInteractPanel(panelDetails);
            TutorialManager.instance.ShowHintById("Campfire_1");
        }
    }
  
    void startBakeIenumerator()
    {
        TutorialManager.instance.ShowHintById("Campfire_Bake_1");
        if (!IsFire)
        {
            GUIManager.instance.AppendLogLine("First, you have to ignite campfire.");
            return;
        }
        HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
        if (PartyController.instance.IsAnyoneBaking)
            return;
        characterController.CurrentCoroutine = Bake(characterController);
        characterController.StartCurrentCoroutine();
    }
   
    public IEnumerator Bake(HumanController characterController)
    {
        progress = 0;
        List<GameObject> rawFood = GameManager.instance.GetRawFood();
        if (rawFood.Count == 0)
        {
            GUIManager.instance.AppendLogLine("You don't have any food to bake.");
            yield break;
        }
        if (!CanPerformAction())
        {
            yield break;
        }
        if (characterController == null) yield break;
        characterController.SetActivity(Activity.baking);
        Vector3 startPosition = characterController.transform.position;
        characterController.GoTo(interactPlace);
        while (!characterController.CheckIfTargetArrived())
        {
            yield return new WaitForSeconds(0.1f);
        }
        characterController.SetAnimTrigger("DoSmth");
        characterController.Play(characterController.RustleSound);

        while (progress < 100)
        {
            progress += 1;
            characterController.UpdateProgressBar(progress);
            yield return new WaitForSeconds(0.1f);
        }
        characterController.Stop();
        characterController.SetAnimTrigger("EndDoSmth");
        yield return new WaitForSeconds(1f);

        foreach (GameObject item in GameManager.instance.GetRawFood())
        {
            //Create new item
            var newItem = ItemFactory.CreateItem(2);
            newItem.transform.SetParent(item.transform.parent);
            newItem.GetComponentInChildren<Text>().text = item.GetComponentInChildren<Text>().text;

            GameManager.instance.RemoveItemFromEquipment(item);
            DestroyImmediate(item);
        }
        characterController.GoTo(startPosition);
        characterController.SetActivityAsIdle();
    }

    void ShowIgnitePanel()
    {
        TutorialManager.instance.ShowHintById("Campfire_Ignite_1");
        campfirePanel.SetActive(true);
    }

    public void startIgniteIenumerator()
    {
        HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
        if (PartyController.instance.IsAnyoneIgniting)
            return;
        characterController.CurrentCoroutine = Ignite(characterController);
        characterController.StartCurrentCoroutine();
    }

    IEnumerator Ignite(HumanController characterController)
    {
        progress = 0;
        if (!CanPerformAction())
            yield break;
        if (characterController == null)
            yield break;
        characterController.SetActivity(Activity.igniting);
        Vector3 startPosition = characterController.transform.position;
        characterController.GoTo(interactPlace);
        while (!characterController.CheckIfTargetArrived())
        {
            yield return new WaitForSeconds(0.1f);
        }
        characterController.SetAnimTrigger("DoSmth");
        characterController.Play(characterController.RustleSound);
        while (progress < 100)
        {
            progress += 5;
            characterController.UpdateProgressBar(progress);
            yield return new WaitForSeconds(0.1f);
        }
        characterController.Stop();
        characterController.SetAnimTrigger("EndDoSmth");
        yield return new WaitForSeconds(1f);
        characterController.GoTo(startPosition);
        characterController.SetActivityAsIdle();
        StartCoroutine(CampfireBurn());
    }

    IEnumerator CampfireBurn()
    {
        IsFire = true;
        animator.SetBool("IsFire", true);
        Play(fireClip, true);
        while (IsWood())
        {
            progress = 0;
            int burnTime = (campfireSlot.GetComponentInChildren<ItemController>().ItemProperties as WoodProperties).BurnTime;
            while (progress <= burnTime)
            {
                if (!IsFire) yield break;
                campfireTime.text = "Time: " + (burnTime - progress);
                progress += 1;
                yield return new WaitForSeconds(0.1f);
            }
        }
        GUIManager.instance.AppendLogLine("The campfire went out.");
        IsFire = false;
        animator.SetBool("IsFire", false);
        Stop();
    }

    public bool IsWood()
    {
        if (campfireSlot.transform.childCount > 0 && IsFire)
        {
            var childText = campfireSlot.GetComponentInChildren<Text>();
            int count = Convert.ToInt32(childText.text);
            if (count == 1)
            {
                GameManager.instance.RemoveItemFromEquipment(campfireSlot.transform.GetChild(0).gameObject);
                Destroy(campfireSlot.transform.GetChild(0).gameObject);
            }
            else
                count--;
            childText.text = count.ToString();
            return true;
        }
        return false;
    }

    public void ResetCampfire()
    {
        IsFire = false;
        campfireTime.text = "Time: 0";
        campfirePanel.SetActive(false);
        animator.SetBool("IsFire", false);
        Stop();
    }

}
