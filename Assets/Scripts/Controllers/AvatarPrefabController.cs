﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AvatarPrefabController : MonoBehaviour, IDropHandler
{

    public HumanController HumanController;
    public GameObject EquipmentPanel;
    private Image avatar;
    private GameObject trash;

    void Start()
    {
        EquipmentPanel = GameObject.Find("Canvas").transform.Find("EquipmentPanel").gameObject;
        trash = GameObject.Find("Trash");
        foreach (var av in EquipmentPanel.GetComponentsInChildren<Image>())
        {
            if (av.name == "Avatar")
            {
                avatar = av;
            }

        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        var itemProperties = eventData.pointerDrag.GetComponent<ItemController>().ItemProperties;
        if (itemProperties.Type != ItemProperties.ItemType.Health) return;
        HumanController.SetHealth(HumanController.GetHealth() + (itemProperties as HealthItemProperties).LifeValue);
        GameManager.instance.RemoveItemFromEquipment(eventData.pointerDrag);
        Destroy(eventData.pointerDrag);
        if(GUIManager.instance.CharacterPanel.activeInHierarchy) GUIManager.instance.UpdateCharacterPanel(HumanController.characterProperties);
    }

    public void OnClick()
    {
        foreach(var child in EquipmentPanel.GetComponentsInChildren<Text>())
        {
            switch (child.name)
            {
                case "CharacterName":
                    //jeśli drugie kliknięcie w ten sam avatar, to zamykamy
                    if (child.text == HumanController.characterProperties.FirstName + " " + HumanController.characterProperties.LastName && EquipmentPanel.activeInHierarchy)
                    {
                        EquipmentPanel.SetActive(false);
                        GUIManager.instance.HideCharacterPanel();
                        return;
                    }
                    child.text = HumanController.characterProperties.FirstName + " " + HumanController.characterProperties.LastName;
                    break;
                case "Age":
                    child.text = "Age: " + HumanController.characterProperties.Age;
                    break;
                case "ActionPoints":
                    child.text = "Exhaustion: " + HumanController.characterProperties.Exhaustion;
                    break;
                case "ActivityTxt":
                    child.text = HumanController.GetActualActivity();
                    break;
            }
        }
        avatar.sprite = Resources.Load<Sprite>(HumanController.characterProperties.Avatar);
        HideOldItems();
        ShowItems();
        EquipmentPanel.SetActive(true);
        GameManager.instance.SetSelectedCharacter(HumanController);
        GUIManager.instance.UpdateCharacterPanel(HumanController.characterProperties);
    }

    private void HideOldItems()
    {
        foreach (var child in EquipmentPanel.GetComponentsInChildren<Image>())
        {
            if(child.name == "CloseButton" || child.name == "EquipmentPanel" || child.transform.childCount == 0) continue;
                child.transform.GetChild(0).SetParent(trash.transform);
        }
    }

    private void ShowItems()
    {
        foreach (var child in EquipmentPanel.GetComponentsInChildren<Image>())
        {
            if (child.name == "LeftHandSlot" && HumanController.LeftHandWeapon != null)
            {
                HumanController.LeftHandWeapon.transform.SetParent(child.transform);
            }
            else if (child.name == "RightHandSlot" && HumanController.RightHandWeapon != null)
            {
                HumanController.RightHandWeapon.transform.SetParent(child.transform);
            }
            else if (child.name == "ArmorSlot" && HumanController.Armor != null)
            {
                HumanController.Armor.transform.SetParent(child.transform);
            }
            else if (child.name == "BootsSlot" && HumanController.Boots != null)
            {
                HumanController.Boots.transform.SetParent(child.transform);
            }
            else if (child.name == "HelmetSlot" && HumanController.Helmet != null)
            {
                HumanController.Helmet.transform.SetParent(child.transform);
            }
        }
    }

}
