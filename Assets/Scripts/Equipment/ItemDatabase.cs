﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour {

    

    private static List<ItemProperties> _items;
    private static bool _isDatabaseLoaded = false;

    private static void ValidateDatabase()
    {
        if (_items == null) _items = new List<ItemProperties>();
        if (!_isDatabaseLoaded) LoadDatabase();
    }

    private static void AddItemToDatabase(ItemProperties item)
    {
        if (!_items.Contains(item))
        {
            _items.Add(item);
        }
    }

    public static void LoadDatabase()
    {
        if (_isDatabaseLoaded) return;
        _isDatabaseLoaded = true;
        LoadDatabaseForce();
    }

    public static void LoadDatabaseForce()
    {
        ValidateDatabase();
        ItemProperties[] resources = Resources.LoadAll<ItemProperties>(@"Items/");
        foreach (ItemProperties item in resources)
        {
            AddItemToDatabase(item);
        }
    }

    public static void ClearDatabase()
    {
        _isDatabaseLoaded = false;
        _items.Clear();
    }

    public static ItemProperties GetItem(int id)
    {
        ValidateDatabase();
        foreach (ItemProperties item in _items)
        {
            if (item.Id == id)
            {
                return Instantiate(item);
            }
                
        }
        return null;
    }

    public static List<ItemProperties> GetItemList
    {
        get
        {
            ValidateDatabase();
            return _items;
        }
    }

}
