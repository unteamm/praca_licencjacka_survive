﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemProperties : ScriptableObject
{
    public int Id = 0;
    public Sprite Image;
    public string Name;
    public string Description;
    public bool IsStackable;

    public enum ItemType
    {
        Weapon, RawFood, Food, Health, Armor, Helmet, Boots, Wood
    }

    public ItemType Type;

}
