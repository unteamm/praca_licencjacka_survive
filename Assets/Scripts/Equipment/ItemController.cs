﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public static ItemController Instance;

    public static GameObject ItemBeingDragged;
    public ItemProperties ItemProperties;
    private GameObject startParent;
    private GameObject itemDescriptionPanel;
    private GameObject canvas;
    private Vector3 _startPosition;

    void Start()
    {
        itemDescriptionPanel = GameObject.Find("Canvas").transform.Find("ItemDescriptionPanel").gameObject;
        canvas = GameObject.Find("Canvas").gameObject;
        Instance = this;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        ItemBeingDragged = gameObject;
        startParent = ItemBeingDragged.transform.parent.gameObject;
        ItemBeingDragged.transform.SetParent(canvas.transform);
        ItemBeingDragged.transform.SetAsLastSibling();
        //_startPosition = transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        ItemBeingDragged.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (ItemBeingDragged.transform.parent == canvas.transform)
            ResetItemBeingDraggedPosition();
            
        ItemBeingDragged = null;
        //transform.position = _startPosition;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (ItemBeingDragged != null) return;
        float width = eventData.pointerEnter.GetComponent<RectTransform>().rect.width;
        float height = eventData.pointerEnter.GetComponent<RectTransform>().rect.height;
        itemDescriptionPanel.GetComponentInChildren<Text>().text = ItemProperties.Description;
        var position = eventData.pointerEnter.transform.position;
        itemDescriptionPanel.transform.position = new Vector3(position.x + width/2, position.y + height/2, position.z);
        itemDescriptionPanel.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        itemDescriptionPanel.SetActive(false);
    }

    public void ResetItemBeingDraggedPosition()
    {
        if (startParent == null) return;
        ItemBeingDragged.transform.SetParent(startParent.transform);
        ItemBeingDragged.transform.localPosition = Vector3.zero;
        startParent = null;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right &&
            eventData.pointerDrag.GetComponent<ItemController>().ItemProperties.IsStackable &&
            eventData.pointerDrag.GetComponentInChildren<Text>().text != "1" &&
            eventData.pointerDrag.GetComponentInChildren<Text>().text != "")
        {
            ItemBeingDragged = ItemFactory.CreateItem(eventData.pointerDrag.GetComponent<ItemController>().ItemProperties.Id);
            this.GetComponentInChildren<Text>().text = (Convert.ToInt32(this.GetComponentInChildren<Text>().text) - 1).ToString();
        }
    }
}

