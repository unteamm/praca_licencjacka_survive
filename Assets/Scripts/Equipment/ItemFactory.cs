﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemFactory : MonoBehaviour
{

    public static GameObject CreateItem(int id, bool setParent = true)
    {
        ItemProperties item = ItemDatabase.GetItem(id);
        
        GameObject obj = Instantiate(Resources.Load<GameObject>(@"Prefabs/ItemPrefab"));
        var objImage = obj.GetComponent<Image>();
        var propertyContainer = obj.GetComponent<PropertyContainer>();
        var itemController = obj.GetComponent<ItemController>();
        var countText = obj.transform.GetComponentInChildren<Text>();
        itemController.ItemProperties = item;
        propertyContainer.ScriptableObject = item;
        objImage.sprite = item.Image;
        objImage.preserveAspect = true;
        obj.name = item.Name;

        if(item.IsStackable)
            countText.text = "1";

        GameManager.instance.AddItemToEquipment(obj);

        var equipment = EquipmentController.instance.Equipment;
        for (int i = 0; i < equipment.transform.childCount; i++)
        {
            var child = equipment.transform.GetChild(i);
            if (child.childCount != 0) continue;
            obj.transform.SetParent(child);
            break;
        }
        return obj;
    }

}
