﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WearableItemProperties : ItemProperties {

    public int HealthToAdd;
    public int FightingToAdd;
    public int ExplorationToAdd;
    public int MoraleToAdd;
    public int ActionPointsToAdd;
}
