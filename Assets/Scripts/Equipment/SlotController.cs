﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotController : MonoBehaviour, IDropHandler
{
    public bool isCampfireSlot;
    public bool isFeedingSlot;
    public bool isHelmet;
    public bool isArmor;
    public bool isWeapon;
    public bool isBoots;

    private Image armorSlot;
    private Image leftHandSlot;
    private Image rightHandSlot;
    private Image helmetSlot;
    private Image bootsSlot;

    void Start()
    {
        foreach (var child in AvatarPanelController.Instance.EquipmentPanel.GetComponentsInChildren<Image>())
        {
            switch (child.name)
            {
                case "ArmorSlot":
                    armorSlot = child;
                    break;
                case "LeftHandSlot":
                    leftHandSlot = child;
                    break;
                case "RightHandSlot":
                    rightHandSlot = child;
                    break;
                case "HelmetSlot":
                    helmetSlot = child;
                    break;
                case "BootsSlot":
                    bootsSlot = child;
                    break;
            }
        }
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        var actualHuman = GetActualHuman();
        var itemProperties = ItemController.ItemBeingDragged.GetComponent<PropertyContainer>().ScriptableObject as ItemProperties;
        if (itemProperties == null) return;
        //STACKABLE ITEMS
        if (transform.childCount > 0)
        {
            var childProperties = transform.GetChild(0).GetComponent<ItemController>().ItemProperties;
            if (itemProperties.IsStackable && childProperties.Id == itemProperties.Id &&
                ItemController.ItemBeingDragged != transform.GetChild(0).gameObject && !isFeedingSlot)
            {
                var childText = transform.GetChild(0).transform.GetComponentInChildren<Text>();
                childText.text =
                (Convert.ToInt32(childText.text) +
                 Convert.ToInt32(ItemController.ItemBeingDragged.GetComponentInChildren<Text>().text)).ToString();
                GameManager.instance.RemoveItemFromEquipment(ItemController.ItemBeingDragged);
                Destroy(ItemController.ItemBeingDragged);
            }
            else
                ItemController.Instance.ResetItemBeingDraggedPosition();
        }
        //FEEDING SLOT
        else if (isFeedingSlot)
        {
            if (itemProperties.Type == ItemProperties.ItemType.RawFood)
            {
                GUIManager.instance.AppendLogLine("You must first bake the food.");
                ItemController.Instance.ResetItemBeingDraggedPosition();
            }
            else if (itemProperties.Type != ItemProperties.ItemType.Food)
                ItemController.Instance.ResetItemBeingDraggedPosition();
            else if (ItemController.ItemBeingDragged.GetComponentInChildren<Text>().text != "1")
            {
                GUIManager.instance.AppendLogLine("Too many food.");
                ItemController.Instance.ResetItemBeingDraggedPosition();
            }
            else
            {
                ItemController.ItemBeingDragged.transform.SetParent(transform);
            }
        }
        //EQUIPMENT SLOTS
        else if (isHelmet)
        {
            if (itemProperties.Type != ItemProperties.ItemType.Helmet)
                ItemController.Instance.ResetItemBeingDraggedPosition();
            else
            {
                ItemController.ItemBeingDragged.transform.SetParent(transform);
                actualHuman.AddToCharacterEquipment(ItemController.ItemBeingDragged);
            }
        }
        else if (isArmor)
        {
            if (itemProperties.Type != ItemProperties.ItemType.Armor)
                ItemController.Instance.ResetItemBeingDraggedPosition();
            else
            {
                ItemController.ItemBeingDragged.transform.SetParent(transform);
                actualHuman.AddToCharacterEquipment(ItemController.ItemBeingDragged);
            }
        }
        else if (isWeapon)
        {
            if (itemProperties.Type != ItemProperties.ItemType.Weapon)
                ItemController.Instance.ResetItemBeingDraggedPosition();
            else
            {
                ItemController.ItemBeingDragged.transform.SetParent(transform);
                if (eventData.pointerEnter.name == "LeftHandSlot")
                    actualHuman.AddToCharacterEquipment(ItemController.ItemBeingDragged, true);
                else
                {
                    actualHuman.AddToCharacterEquipment(ItemController.ItemBeingDragged);
                }
            }
        }
        else if (isBoots)
        {
            if (itemProperties.Type != ItemProperties.ItemType.Boots)
                ItemController.Instance.ResetItemBeingDraggedPosition();
            else
            {
                ItemController.ItemBeingDragged.transform.SetParent(transform);
                actualHuman.AddToCharacterEquipment(ItemController.ItemBeingDragged);
            }
        }
        //CAMPFIRE SLOT
        else if (isCampfireSlot)
        {
            if (itemProperties.Type != ItemProperties.ItemType.Wood)
            {
                GUIManager.instance.AppendLogLine("This is not a wood.");
                ItemController.Instance.ResetItemBeingDraggedPosition();
            }
            else
                ItemController.ItemBeingDragged.transform.SetParent(transform);
        }
        //ALL SLOTS
        else
            ItemController.ItemBeingDragged.transform.SetParent(transform);

        CheckSlot(eventData.pointerDrag);
    }

    private void CheckSlot(GameObject actualGameObject)
    {
        if (!AvatarPanelController.Instance.EquipmentPanel.activeInHierarchy) return;
        var actualHuman = GetActualHuman();

        if (actualHuman.RightHandWeapon != null && rightHandSlot.transform.childCount == 0)
            actualHuman.RemoveFromCharacterEquipment(actualGameObject);
        else if (actualHuman.LeftHandWeapon != null && leftHandSlot.transform.childCount == 0)
            actualHuman.RemoveFromCharacterEquipment(actualGameObject, true);
        else if (actualHuman.Boots != null && bootsSlot.transform.childCount == 0)
            actualHuman.RemoveFromCharacterEquipment(actualGameObject);
        else if (actualHuman.Armor != null && armorSlot.transform.childCount == 0)
            actualHuman.RemoveFromCharacterEquipment(actualGameObject);
        else if (actualHuman.Helmet != null && helmetSlot.transform.childCount == 0)
            actualHuman.RemoveFromCharacterEquipment(actualGameObject);
    }

    private HumanController GetActualHuman()
    {
        foreach (var humanContoller in PartyController.instance.CharactersInCamp)
        {
            var humanPropertiese = humanContoller.GetCharacterProperties();
            if (AvatarPanelController.Instance.EquipmentPanel.GetComponentInChildren<Text>().text ==
                humanPropertiese.FirstName + " " + humanPropertiese.LastName)
                return humanContoller;
        }
        return null;
    }

}