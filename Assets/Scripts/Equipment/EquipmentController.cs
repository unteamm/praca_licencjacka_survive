﻿using UnityEngine;
using UnityEngine.UI;

public class EquipmentController : MonoBehaviour
{
    public static EquipmentController instance;

    public GameObject Equipment;
    private bool _visibility = false;

    void Start()
    {
        instance = this;
        Equipment.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown("i"))
        {
            ChangeVisibility();
        }
    }

    public void Hide()
    {
        _visibility = false;
        Equipment.SetActive(false);
    }

    public void Show()
    {
        _visibility = true;
        Equipment.SetActive(true);
    }

    public void ChangeVisibility()
    {
        if (ItemController.ItemBeingDragged != null) return;
        _visibility = !_visibility;
        Equipment.SetActive(_visibility);
        if (ItemController.Instance != null)
            ItemController.Instance.OnPointerExit(null);
    }

}
