﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pathfinding : MonoBehaviour {
    public static Pathfinding instance;

    Grid grid;

    public void Awake()
    {
        grid = GetComponent<Grid>();
        instance = this;
     
    }


	public Queue<Node> FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Node startNode = grid.WorldPosToNode(startPos);
        Node endNode = grid.WorldPosToNode(targetPos);
        List<Node> openList = new List<Node>();
        List<Node> closedList = new List<Node>();
        Node node_current = new Node(false,Vector3.zero,Vector2.zero);
        startNode.hCost = GetDistance(startNode, endNode);
        startNode.gCost = 0;
        startNode.fCost = startNode.gCost + startNode.hCost;
        openList.Add(startNode);
        while (openList.Count > 0)
        {
            node_current = GetMinFCost(openList);
            openList.Remove(node_current);
            if(node_current.GridPosition == endNode.GridPosition)
            {
                break;
            }
            foreach (Node node_successor in grid.GetNeighbours(node_current))
            {
                if (!node_successor.Walkable)
                {
                    continue;
                }
                int successor_current_cost = node_current.gCost + GetDistance(node_current, node_successor);
                if (openList.Contains(node_successor))
                {
                    if(node_successor.gCost <= successor_current_cost)
                    {
                        continue;
                    }
                }else if (closedList.Contains(node_successor))
                {
                    if(node_successor.gCost <= successor_current_cost)
                    {
                        continue;
                    }
                    closedList.Remove(node_successor);
                    openList.Add(node_successor);
                }else
                {
                    openList.Add(node_successor);
                    node_successor.hCost = GetDistance(node_successor, endNode);
                }
                node_successor.gCost = successor_current_cost;
                node_successor.parent = node_current;
            }
            closedList.Add(node_current);
        }
        if(node_current.GridPosition != endNode.GridPosition)
        {
            Queue<Node> _path = new Queue<Node>();
            _path.Enqueue(node_current);
            _path.Enqueue(endNode);
            grid.AddToPathNode(_path);
            Debug.LogError("Error in pathfinding");
            return null;
        }
       
        Queue<Node> path = new Queue<Node>();

        Node currentNode = endNode;
        while(currentNode.GridPosition != startNode.GridPosition)
        {
            path.Enqueue(currentNode);
            currentNode = currentNode.parent;
        }
        path = new Queue<Node>(path.Reverse());
        grid.AddToPathNode(path);

        return path;

    }

    public Vector3 GetClosestWalkablePosition(Vector3 pos)
    {
        if (grid.WorldPosToNode(pos).Walkable)
        {
            return pos;
        }
        List<Node> neighbours = grid.GetNeighbours(grid.WorldPosToNode(pos));
        foreach (Node n in neighbours)
        {
            if (n.Walkable)
            {
                return n.WorldPosition;
            }
        }
        return pos;
    }

    Node GetMinFCost(List<Node> nodelist)
    {
        Node temp = nodelist.FirstOrDefault();
        foreach(Node n in nodelist)
        {
            if(temp.fCost > n.fCost)
            {
                temp = n;
            }
        }
        return temp;
    }

    public Vector2 GetGridPos(Vector3 _pos)
    {
        return grid.WorldPosToNode(_pos).GridPosition;
    }

    public bool CheckIfVectorsAreOnTheSameTile(Vector3 _position1, Vector3 _position2)
    {
        Node node1 = grid.WorldPosToNode(_position1);
        Node node2 = grid.WorldPosToNode(_position2);
        if(node1.GridPosition == node2.GridPosition)
        {
            return true;
        }else
        {
            return false;
        }
    }

    int GetDistance(Node a, Node b)
    {
        int dstX = Mathf.Abs(Mathf.RoundToInt(a.GridPosition.x - b.GridPosition.x));
        int dstY = Mathf.Abs(Mathf.RoundToInt(a.GridPosition.y - b.GridPosition.y));

        return Mathf.RoundToInt(Mathf.Sqrt(dstX * dstX + dstY * dstY));
    }

    public void RegenerateGrid()
    {
        grid.CreateGrid();
    }
}
