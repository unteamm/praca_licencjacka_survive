﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Grid : MonoBehaviour {

    public bool DisplayGridGizmos;
    public Vector2 GridSize;
    public float NodeSize;
    public LayerMask UnwalkableMask;


    Node[,] grid;

    int gridSizeX, gridSizeY;

    void Awake()
    {
        gridSizeX = Mathf.RoundToInt(GridSize.x / (NodeSize * 2));
        gridSizeY = Mathf.RoundToInt(GridSize.y / (NodeSize * 2));
        CreateGrid();
    }

    public Node WorldPosToNode(Vector3 pos)
    {
        float percentX = (pos.x + GridSize.x / 2) / GridSize.x;
        float percentY = (pos.y + GridSize.y / 2) / GridSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];
    }

    public void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 gridStartingPoint = transform.position - Vector3.right * GridSize.x / 2 - Vector3.up * GridSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 NodePosition = gridStartingPoint + Vector3.right * (x * NodeSize * 2 + NodeSize) + Vector3.up * (y * NodeSize * 2 + NodeSize);

                bool walkable = !(Physics2D.OverlapCircle(NodePosition, NodeSize,UnwalkableMask));



                grid[x, y] = new Node(walkable, NodePosition, new Vector2(x, y));
            }
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int xx = -1; xx <= 1; xx++)
        {
            for (int yy = -1; yy <= 1; yy++)
            {
                if (xx == 0 && yy == 0)
                {
                    continue; 
                }

                int rx = Mathf.RoundToInt(node.GridPosition.x) + xx;
                int ry = Mathf.RoundToInt(node.GridPosition.y) + yy;

                if (rx >= 0 && ry >= 0 && rx < gridSizeX && ry < gridSizeY)
                {
                    neighbours.Add(grid[rx, ry]);
                }

            }
        }

        return neighbours;
    }

    List<Node> Pathnode = new List<Node>();

    public void AddToPathNode(Queue<Node> node)
    {
        Pathnode.AddRange(node.ToArray().ToList());
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(GridSize.x, GridSize.y, 1));
        if (grid != null && DisplayGridGizmos)
        {
            foreach (Node n in grid)
            {
                if (Pathnode.Contains(n))
                {
                    Gizmos.color = Color.green;
                }else
                {
                    Gizmos.color = (n.Walkable) ? Color.white : Color.red;
                }
                Gizmos.DrawCube(n.WorldPosition, Vector3.one * (NodeSize*2 - .1f));
            }
        }
    }
}
