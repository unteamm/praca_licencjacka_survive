﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{

    public bool Walkable;
    public Vector3 WorldPosition;
    public Vector2 GridPosition;

    public int fCost;
    public int gCost;
    public int hCost;
    public Node parent;


    public Node(bool _walkable, Vector3 _worldPosition, Vector2 _gridPosition)
    {
        Walkable = _walkable;
        WorldPosition = _worldPosition;
        GridPosition = _gridPosition;
    }
}