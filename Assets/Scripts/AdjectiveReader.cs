﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AdjectiveReader
{

    static string fileName = "adjectives";
    static List<string> adjectives;
    
    static AdjectiveReader()
    {
        adjectives = LoadFromFile();
    }

    public static void RepopulateList()
    {
        adjectives = LoadFromFile();
    }

    static List<string> LoadFromFile()
    {
        TextAsset ta = (TextAsset)Resources.Load(fileName, typeof(TextAsset));
        return ta.text.Split('\n')
            .Select(x =>
            {
                x.Trim();
                return (char.ToUpper(x[0]) + x.Substring(1));
            }).ToList();
    }

    public static string GetRandomAdjective()
    {
        int rnd = Random.Range(0, adjectives.Count);
        string adjective = adjectives[rnd];
        adjectives.RemoveAt(rnd);
        return adjective;
    }
}
