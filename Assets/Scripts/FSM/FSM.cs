﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM {
    public Dictionary<string,FSMState> states;
    Dictionary<FSMState, FSMState> transitions; 
    FSMState currentState;
    int stateIndex;

    public FSM(FSMState[] _states)
    {
        states = new Dictionary<string, FSMState>();
        transitions = new Dictionary<FSMState, FSMState>();
        for (int i = 0; i < _states.Length; i++)
        {
            states.Add(_states[i].GetType().Name,_states[i]);
        }
        stateIndex = 0;
        currentState = states["NewCampState"];
    }

    public void Run()
    {
        if(currentState == null)
        {
            currentState = states["PlayerTurnState"];
        }

        if (currentState.Run())
        {
            NextState();
        }
    }

    public void AddTransition(FSMState _current, FSMState _next)
    {
        transitions.Add(_current, _next);
    }

    public void ChangeState(FSMState state)
    {
        currentState = state;
    }

    public void NextState()
    {
        FSMState nextState;
        
        if(!transitions.TryGetValue(currentState, out nextState))
        {
            Debug.LogError("Invalid transition");
            return;
        }

        currentState = nextState;
                
    }

}
