﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleState : FSMState
{
    new void Awake()
    {
        base.Awake();
    }

    public override bool EndStateAction()
    {
        Debug.Log("Battle End State");
        TimeManager.instance.EndBattleMode();
        NightManager.instance.DespawnEnemies();
        GameManager.instance.EnablePlayerInput();
        return true;
    }

    public override bool EnterStateAction()
    {
        Debug.Log("Battle Enter State");
        TimeManager.instance.BeginBattleMode();
        NightManager.instance.isFightOver = false;

        GameManager.instance.DisablePlayerInput();

        TimeManager.instance.SetNormalSpeed();
        StartCoroutine(NightManager.instance.StartFightEnumerator());
       // NightManager.instance.StartNight();
        //Przeniesienie tymczasowo do feeding controller
        //   NightManager.instance.StartNight();
        return true;
    }

    public override bool StateAction()
    {
        //Debug.Log("BattleState Action");
        if (NightManager.instance.isFightOver)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
