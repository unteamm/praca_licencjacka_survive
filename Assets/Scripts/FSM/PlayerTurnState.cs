﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerTurnState : FSMState {
    TimeManager timeManager;

    public GameObject BackgroundNightSprite
    {
        get
        {
            return GameObject.Find("Background").GetComponentsInChildren<Transform>(true).Where(x => x.gameObject.name == "Background_night").FirstOrDefault().gameObject;
        }
    }
    public GameObject BackgroundDaySprite
    {
        get
        {
            return GameObject.Find("Background").GetComponentsInChildren<Transform>(true).Where(x => x.gameObject.name == "Background_day").FirstOrDefault().gameObject;
        }
    }

    DateTime endStateTime = new DateTime(1995, 11, 22, 22, 00, 00);
    new void Awake()
    {
        base.Awake();
        timeManager = GetComponent<TimeManager>();
    }


    public override bool EndStateAction()
    {
        Debug.Log("End Of State");
        return true;
    }

    public override bool EnterStateAction()
    {
        Debug.Log("PlayerTurn Enter State");
        //TODO: Sprawdzenie czy postać wróciła do obozu
        if (GameManager.instance.GetCountOfScavengers() > 0)
        {
            int i = GameManager.instance.GetCountOfScavengers();
            for(int j=0;j< i; j++)
            {
                GameManager.instance.RemoveScavenger(GameManager.instance.GetScavengersList()[j]);
            }
        }

        //TODO: Zmienić na coś bardziej optymalnego. Zakończenie akcji z poprzedniego dnia
        for (int i = 0; i < PartyController.instance.CharactersInstances.Count; i++)
        {
            if (PartyController.instance.CharactersInstances[i].transform.FindChild("Graphics").gameObject.active == false)
            {
                PartyController.instance.CharactersInstances[i].GetComponent<HumanController>().ShowCharacterGraphics();
                PartyController.instance.CharactersInstances[i].GetComponent<HumanController>().ReturnToDefaultPosition();
                PartyController.instance.CharactersInstances[i].GetComponent<HumanController>().SetActivityAsIdle();
            }
        }

        /*
        foreach(HumanProperties properties in PartyController.instance.characters)
        {
            properties.ActionPoints = 2;
        }
        */

        timeManager.InitializeNewDay();
        BackgroundDaySprite.SetActive(true);
        BackgroundNightSprite.SetActive(false);

        string previousCrisisResult = "";

        //Kryzys
        if (CrisisManager.instance.ActiveCrisis != null)
        {
            previousCrisisResult = CrisisManager.instance.CheckCrisis();
        }
        CrisisManager.instance.SetNewActiveCrisis(previousCrisisResult);

        GameManager.instance.EnablePlayerInput();
        return true;
    }

    public override bool StateAction()
    {
        if (timeManager.TimeEquals(endStateTime))
        {
            return true;
        }
        else {
            return false;
        }
    }
}
