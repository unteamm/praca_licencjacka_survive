﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSMState : MonoBehaviour {
    List<Func<bool>> Actions;
    Func<bool> currentAction;
    int index;

    public abstract bool EnterStateAction();
    public abstract bool StateAction();
    public abstract bool EndStateAction();

    protected void Awake()
    {
        Actions = new List<Func<bool>>();
        Actions.Add(EnterStateAction);
        Actions.Add(StateAction);
        Actions.Add(EndStateAction);
        index = 0;
        currentAction = Actions[0];
    }


    public bool Run()
    {
        if (currentAction.Invoke())
        {
            index++;
            if(index>= Actions.Count)
            {
                index = 0;
                currentAction = Actions[index];
                return true;
            }
            currentAction = Actions[index];
        }
        return false;
    }
	
}
