﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCampState : FSMState
{

    public override bool EndStateAction()
    {
        Debug.Log("NewCamp End State");
        return true;
    }

    public override bool EnterStateAction()
    {
        PartyController.instance.SpawnAllCharacters();
        if (PlayerPrefs.HasKey("Load") && PlayerPrefs.GetInt("Load") == 1)
        {
            PlayerPrefs.DeleteKey("Load");
            SaveManager.instance.LoadGame("save.dat");
        }
        return true;
    }

    public override bool StateAction()
    {
        Debug.Log("NewCamp State");
        return true;
    }
}
