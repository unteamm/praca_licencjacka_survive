﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NightState : FSMState
{
    public Image LoadImg;
    new void Awake()
    {
        base.Awake();
    }

    public override bool EndStateAction()
    {
        if (LoadImg.color.a > 0f)
        {
            Color color = LoadImg.color;
            color.a -= 0.01f;
            // fade += 0.06f;
            LoadImg.color = color;
            return false;
        }
        else
        {
            TimeManager.instance.SetNormalSpeed();
            LoadImg.gameObject.SetActive(false);
            return true;
        }
    }

    public override bool EnterStateAction()
    {
        Debug.Log("Night Enter State");
        CampfireController.instance.ResetCampfire();
        TimeManager.instance.SetNormalSpeed();
        FeedingControler.instance.Show();
        GameManager.instance.DisablePlayerInput();
        return true;
    }

    public override bool StateAction()
    {
        if (FeedingControler.instance.Feeding.active == true)
        {
            return false;
        }
        else
        {
            LoadImg.gameObject.SetActive(true);
            if (LoadImg.color.a < 1f)
            {
                Color color = LoadImg.color;
                color.a += 0.01f;
                // fade += 0.06f;
                LoadImg.color = color;
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
