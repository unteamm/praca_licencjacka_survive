﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemTestScripts : MonoBehaviour {

	// Use this for initialization
    void Update()
    {
        //Bake
        if (Input.GetKeyDown("space"))
        {
            //CampfireController c = GetComponent<CampfireController>();
            HumanController characterController = (HumanController)GameManager.instance.GetSelectedCharacter();
            //StartCoroutine(c.Bake(characterController));
            //Debug.Log((TimeManager.instance.GetTime().TimeOfDay.TotalSeconds - 36000) /(79200-36000));
            //Debug.Log(79200);
            //StartCoroutine(characterController.UpdateProgressBarToNight());
        }

        //Create Item
        if (Input.GetKeyDown("t"))
        {
            ItemFactory.CreateItem(1);
            ItemFactory.CreateItem(2);
            ItemFactory.CreateItem(3);
            ItemFactory.CreateItem(4);
            ItemFactory.CreateItem(5);
            ItemFactory.CreateItem(6);
            ItemFactory.CreateItem(7);
            ItemFactory.CreateItem(8);
            ItemFactory.CreateItem(9);
            ItemFactory.CreateItem(10);
            ItemFactory.CreateItem(11);
            ItemFactory.CreateItem(12);
        }

        if (Input.GetKeyDown("f"))
        {
            ItemFactory.CreateItem(1);
        }

        if (Input.GetKeyDown("w"))
        {
            ItemFactory.CreateItem(12);
        }


    }

}
