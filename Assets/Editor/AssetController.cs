﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetController : MonoBehaviour {

    [MenuItem("Assets/Create/Food")]
    public static void CreateFood()
    {
        ScriptableObjectUtility.CreateAsset<FoodProperties>();
    }

    [MenuItem("Assets/Create/WearableItem")]
    public static void CreateWeapon()
    {
        ScriptableObjectUtility.CreateAsset<WearableItemProperties>();
    }

    [MenuItem("Assets/Create/HealthItem")]
    public static void CreateKit()
    {
        ScriptableObjectUtility.CreateAsset<HealthItemProperties>();
    }

    [MenuItem("Assets/Create/Wood")]
    public static void CreateWood()
    {
        ScriptableObjectUtility.CreateAsset<WoodProperties>();
    }

}
