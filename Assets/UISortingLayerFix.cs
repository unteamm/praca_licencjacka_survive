﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISortingLayerFix : MonoBehaviour {

    public string SortingLayerName;
    public int SortingOrder;
	// Use this for initialization
	void Awake () {
        this.gameObject.GetComponent<Canvas>().sortingLayerName = SortingLayerName;
        this.gameObject.GetComponent<Canvas>().sortingOrder = SortingOrder;
    }

}
